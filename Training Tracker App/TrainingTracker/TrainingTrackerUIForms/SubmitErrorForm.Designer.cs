﻿namespace TrainingTrackerUIForms
{
    partial class SubmitErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubmitErrorForm));
            this.dateOfErrorLabelSubmitErrorForm = new System.Windows.Forms.Label();
            this.procedureLabelSubmitErrorForm = new System.Windows.Forms.Label();
            this.procedureComboBoxSubmitErrorForm = new System.Windows.Forms.ComboBox();
            this.descriptionOfIncidentLabelSubmitErrorForm = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.submitButtonSubmitErrorForm = new System.Windows.Forms.Button();
            this.associateLabelSubmitErrorForm = new System.Windows.Forms.Label();
            this.associateComboBoxSubmitErrorForm = new System.Windows.Forms.ComboBox();
            this.reporterLabelSubmitErrorForm = new System.Windows.Forms.Label();
            this.reporterComboBoxSubmitErrorForm = new System.Windows.Forms.ComboBox();
            this.datePortionDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.timeOfErrorLabelSubmitError = new System.Windows.Forms.Label();
            this.timePortionDateTimePickerSubmitError = new System.Windows.Forms.DateTimePicker();
            this.severityLabelSubmitError = new System.Windows.Forms.Label();
            this.severityComboBoxSubmitError = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // dateOfErrorLabelSubmitErrorForm
            // 
            this.dateOfErrorLabelSubmitErrorForm.AutoSize = true;
            this.dateOfErrorLabelSubmitErrorForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dateOfErrorLabelSubmitErrorForm.Location = new System.Drawing.Point(12, 22);
            this.dateOfErrorLabelSubmitErrorForm.Name = "dateOfErrorLabelSubmitErrorForm";
            this.dateOfErrorLabelSubmitErrorForm.Size = new System.Drawing.Size(160, 32);
            this.dateOfErrorLabelSubmitErrorForm.TabIndex = 0;
            this.dateOfErrorLabelSubmitErrorForm.Text = "Date Of Error:";
            // 
            // procedureLabelSubmitErrorForm
            // 
            this.procedureLabelSubmitErrorForm.AutoSize = true;
            this.procedureLabelSubmitErrorForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.procedureLabelSubmitErrorForm.Location = new System.Drawing.Point(538, 22);
            this.procedureLabelSubmitErrorForm.Name = "procedureLabelSubmitErrorForm";
            this.procedureLabelSubmitErrorForm.Size = new System.Drawing.Size(128, 32);
            this.procedureLabelSubmitErrorForm.TabIndex = 2;
            this.procedureLabelSubmitErrorForm.Text = "Procedure:";
            // 
            // procedureComboBoxSubmitErrorForm
            // 
            this.procedureComboBoxSubmitErrorForm.FormattingEnabled = true;
            this.procedureComboBoxSubmitErrorForm.Location = new System.Drawing.Point(672, 19);
            this.procedureComboBoxSubmitErrorForm.Name = "procedureComboBoxSubmitErrorForm";
            this.procedureComboBoxSubmitErrorForm.Size = new System.Drawing.Size(231, 39);
            this.procedureComboBoxSubmitErrorForm.TabIndex = 3;
            // 
            // descriptionOfIncidentLabelSubmitErrorForm
            // 
            this.descriptionOfIncidentLabelSubmitErrorForm.AutoSize = true;
            this.descriptionOfIncidentLabelSubmitErrorForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.descriptionOfIncidentLabelSubmitErrorForm.Location = new System.Drawing.Point(12, 304);
            this.descriptionOfIncidentLabelSubmitErrorForm.Name = "descriptionOfIncidentLabelSubmitErrorForm";
            this.descriptionOfIncidentLabelSubmitErrorForm.Size = new System.Drawing.Size(267, 32);
            this.descriptionOfIncidentLabelSubmitErrorForm.TabIndex = 4;
            this.descriptionOfIncidentLabelSubmitErrorForm.Text = "Description Of Incident:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(18, 355);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1202, 357);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // submitButtonSubmitErrorForm
            // 
            this.submitButtonSubmitErrorForm.Location = new System.Drawing.Point(18, 757);
            this.submitButtonSubmitErrorForm.Name = "submitButtonSubmitErrorForm";
            this.submitButtonSubmitErrorForm.Size = new System.Drawing.Size(154, 87);
            this.submitButtonSubmitErrorForm.TabIndex = 6;
            this.submitButtonSubmitErrorForm.Text = "Submit Error";
            this.submitButtonSubmitErrorForm.UseVisualStyleBackColor = true;
            this.submitButtonSubmitErrorForm.Click += new System.EventHandler(this.SubmitButtonSubmitErrorForm_Click);
            // 
            // associateLabelSubmitErrorForm
            // 
            this.associateLabelSubmitErrorForm.AutoSize = true;
            this.associateLabelSubmitErrorForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.associateLabelSubmitErrorForm.Location = new System.Drawing.Point(538, 110);
            this.associateLabelSubmitErrorForm.Name = "associateLabelSubmitErrorForm";
            this.associateLabelSubmitErrorForm.Size = new System.Drawing.Size(119, 32);
            this.associateLabelSubmitErrorForm.TabIndex = 7;
            this.associateLabelSubmitErrorForm.Text = "Associate:";
            // 
            // associateComboBoxSubmitErrorForm
            // 
            this.associateComboBoxSubmitErrorForm.FormattingEnabled = true;
            this.associateComboBoxSubmitErrorForm.Location = new System.Drawing.Point(672, 107);
            this.associateComboBoxSubmitErrorForm.Name = "associateComboBoxSubmitErrorForm";
            this.associateComboBoxSubmitErrorForm.Size = new System.Drawing.Size(231, 39);
            this.associateComboBoxSubmitErrorForm.TabIndex = 8;
            // 
            // reporterLabelSubmitErrorForm
            // 
            this.reporterLabelSubmitErrorForm.AutoSize = true;
            this.reporterLabelSubmitErrorForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reporterLabelSubmitErrorForm.Location = new System.Drawing.Point(538, 197);
            this.reporterLabelSubmitErrorForm.Name = "reporterLabelSubmitErrorForm";
            this.reporterLabelSubmitErrorForm.Size = new System.Drawing.Size(111, 32);
            this.reporterLabelSubmitErrorForm.TabIndex = 9;
            this.reporterLabelSubmitErrorForm.Text = "Reporter:";
            // 
            // reporterComboBoxSubmitErrorForm
            // 
            this.reporterComboBoxSubmitErrorForm.FormattingEnabled = true;
            this.reporterComboBoxSubmitErrorForm.Location = new System.Drawing.Point(672, 194);
            this.reporterComboBoxSubmitErrorForm.Name = "reporterComboBoxSubmitErrorForm";
            this.reporterComboBoxSubmitErrorForm.Size = new System.Drawing.Size(231, 39);
            this.reporterComboBoxSubmitErrorForm.TabIndex = 10;
            // 
            // datePortionDateTimePicker1
            // 
            this.datePortionDateTimePicker1.CustomFormat = "";
            this.datePortionDateTimePicker1.Location = new System.Drawing.Point(55, 57);
            this.datePortionDateTimePicker1.Name = "datePortionDateTimePicker1";
            this.datePortionDateTimePicker1.Size = new System.Drawing.Size(453, 38);
            this.datePortionDateTimePicker1.TabIndex = 11;
            // 
            // timeOfErrorLabelSubmitError
            // 
            this.timeOfErrorLabelSubmitError.AutoSize = true;
            this.timeOfErrorLabelSubmitError.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.timeOfErrorLabelSubmitError.Location = new System.Drawing.Point(12, 126);
            this.timeOfErrorLabelSubmitError.Name = "timeOfErrorLabelSubmitError";
            this.timeOfErrorLabelSubmitError.Size = new System.Drawing.Size(159, 32);
            this.timeOfErrorLabelSubmitError.TabIndex = 12;
            this.timeOfErrorLabelSubmitError.Text = "Time of Error:";
            // 
            // timePortionDateTimePickerSubmitError
            // 
            this.timePortionDateTimePickerSubmitError.CustomFormat = "";
            this.timePortionDateTimePickerSubmitError.Location = new System.Drawing.Point(55, 174);
            this.timePortionDateTimePickerSubmitError.Name = "timePortionDateTimePickerSubmitError";
            this.timePortionDateTimePickerSubmitError.Size = new System.Drawing.Size(453, 38);
            this.timePortionDateTimePickerSubmitError.TabIndex = 13;
            this.timePortionDateTimePickerSubmitError.ValueChanged += new System.EventHandler(this.DateTimePicker2_ValueChanged);
            // 
            // severityLabelSubmitError
            // 
            this.severityLabelSubmitError.AutoSize = true;
            this.severityLabelSubmitError.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.severityLabelSubmitError.Location = new System.Drawing.Point(538, 279);
            this.severityLabelSubmitError.Name = "severityLabelSubmitError";
            this.severityLabelSubmitError.Size = new System.Drawing.Size(105, 32);
            this.severityLabelSubmitError.TabIndex = 14;
            this.severityLabelSubmitError.Text = "Severity:";
            // 
            // severityComboBoxSubmitError
            // 
            this.severityComboBoxSubmitError.FormattingEnabled = true;
            this.severityComboBoxSubmitError.Location = new System.Drawing.Point(672, 276);
            this.severityComboBoxSubmitError.Name = "severityComboBoxSubmitError";
            this.severityComboBoxSubmitError.Size = new System.Drawing.Size(231, 39);
            this.severityComboBoxSubmitError.TabIndex = 15;
            // 
            // SubmitErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1259, 879);
            this.Controls.Add(this.severityComboBoxSubmitError);
            this.Controls.Add(this.severityLabelSubmitError);
            this.Controls.Add(this.timePortionDateTimePickerSubmitError);
            this.Controls.Add(this.timeOfErrorLabelSubmitError);
            this.Controls.Add(this.datePortionDateTimePicker1);
            this.Controls.Add(this.reporterComboBoxSubmitErrorForm);
            this.Controls.Add(this.reporterLabelSubmitErrorForm);
            this.Controls.Add(this.associateComboBoxSubmitErrorForm);
            this.Controls.Add(this.associateLabelSubmitErrorForm);
            this.Controls.Add(this.submitButtonSubmitErrorForm);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.descriptionOfIncidentLabelSubmitErrorForm);
            this.Controls.Add(this.procedureComboBoxSubmitErrorForm);
            this.Controls.Add(this.procedureLabelSubmitErrorForm);
            this.Controls.Add(this.dateOfErrorLabelSubmitErrorForm);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "SubmitErrorForm";
            this.Text = "SubmitErrorForm";
            this.Load += new System.EventHandler(this.SubmitErrorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dateOfErrorLabelSubmitErrorForm;
        private System.Windows.Forms.Label procedureLabelSubmitErrorForm;
        private System.Windows.Forms.ComboBox procedureComboBoxSubmitErrorForm;
        private System.Windows.Forms.Label descriptionOfIncidentLabelSubmitErrorForm;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button submitButtonSubmitErrorForm;
        private System.Windows.Forms.Label associateLabelSubmitErrorForm;
        private System.Windows.Forms.ComboBox associateComboBoxSubmitErrorForm;
        private System.Windows.Forms.Label reporterLabelSubmitErrorForm;
        private System.Windows.Forms.ComboBox reporterComboBoxSubmitErrorForm;
        private System.Windows.Forms.DateTimePicker datePortionDateTimePicker1;
        private System.Windows.Forms.Label timeOfErrorLabelSubmitError;
        private System.Windows.Forms.DateTimePicker timePortionDateTimePickerSubmitError;
        private System.Windows.Forms.Label severityLabelSubmitError;
        private System.Windows.Forms.ComboBox severityComboBoxSubmitError;
    }
}