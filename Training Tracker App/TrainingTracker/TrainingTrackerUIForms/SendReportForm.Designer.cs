﻿namespace TrainingTrackerUIForms
{
    partial class SendReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendReportForm));
            this.emailAddressListBoxSendReport = new System.Windows.Forms.ListBox();
            this.emailAddressLabelSendReport = new System.Windows.Forms.Label();
            this.emailAddressTextBoxSendReport = new System.Windows.Forms.TextBox();
            this.addEmailAddressButtonSendReport = new System.Windows.Forms.Button();
            this.sendReportButtonSendReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // emailAddressListBoxSendReport
            // 
            this.emailAddressListBoxSendReport.FormattingEnabled = true;
            this.emailAddressListBoxSendReport.ItemHeight = 31;
            this.emailAddressListBoxSendReport.Location = new System.Drawing.Point(12, 137);
            this.emailAddressListBoxSendReport.Name = "emailAddressListBoxSendReport";
            this.emailAddressListBoxSendReport.Size = new System.Drawing.Size(399, 345);
            this.emailAddressListBoxSendReport.TabIndex = 0;
            // 
            // emailAddressLabelSendReport
            // 
            this.emailAddressLabelSendReport.AutoSize = true;
            this.emailAddressLabelSendReport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.emailAddressLabelSendReport.Location = new System.Drawing.Point(21, 36);
            this.emailAddressLabelSendReport.Name = "emailAddressLabelSendReport";
            this.emailAddressLabelSendReport.Size = new System.Drawing.Size(179, 32);
            this.emailAddressLabelSendReport.TabIndex = 1;
            this.emailAddressLabelSendReport.Text = "E-Mail Address:";
            // 
            // emailAddressTextBoxSendReport
            // 
            this.emailAddressTextBoxSendReport.Location = new System.Drawing.Point(201, 33);
            this.emailAddressTextBoxSendReport.Name = "emailAddressTextBoxSendReport";
            this.emailAddressTextBoxSendReport.Size = new System.Drawing.Size(357, 38);
            this.emailAddressTextBoxSendReport.TabIndex = 2;
            // 
            // addEmailAddressButtonSendReport
            // 
            this.addEmailAddressButtonSendReport.Location = new System.Drawing.Point(641, 12);
            this.addEmailAddressButtonSendReport.Name = "addEmailAddressButtonSendReport";
            this.addEmailAddressButtonSendReport.Size = new System.Drawing.Size(151, 79);
            this.addEmailAddressButtonSendReport.TabIndex = 3;
            this.addEmailAddressButtonSendReport.Text = "Add E-Mail";
            this.addEmailAddressButtonSendReport.UseVisualStyleBackColor = true;
            // 
            // sendReportButtonSendReport
            // 
            this.sendReportButtonSendReport.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendReportButtonSendReport.Location = new System.Drawing.Point(583, 261);
            this.sendReportButtonSendReport.Name = "sendReportButtonSendReport";
            this.sendReportButtonSendReport.Size = new System.Drawing.Size(156, 83);
            this.sendReportButtonSendReport.TabIndex = 4;
            this.sendReportButtonSendReport.Text = "Send Report";
            this.sendReportButtonSendReport.UseVisualStyleBackColor = true;
            // 
            // SendReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(910, 538);
            this.Controls.Add(this.sendReportButtonSendReport);
            this.Controls.Add(this.addEmailAddressButtonSendReport);
            this.Controls.Add(this.emailAddressTextBoxSendReport);
            this.Controls.Add(this.emailAddressLabelSendReport);
            this.Controls.Add(this.emailAddressListBoxSendReport);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "SendReportForm";
            this.Text = "SendReportForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox emailAddressListBoxSendReport;
        private System.Windows.Forms.Label emailAddressLabelSendReport;
        private System.Windows.Forms.TextBox emailAddressTextBoxSendReport;
        private System.Windows.Forms.Button addEmailAddressButtonSendReport;
        private System.Windows.Forms.Button sendReportButtonSendReport;
    }
}