﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.IO;

namespace TrainingTrackerUIForms
{
    public partial class SubmitErrorForm : Form
    {
        public SubmitErrorForm()
        {
            InitializeComponent();
            var hubFormReference = new HubForm();
            var associateList = new List<AssociateModel>();
            var severityList = new List<int> { 1, 2, 3, 4 };
            associateList = hubFormReference.LoadAssociates();
            var associateStrings = hubFormReference.AssociateToString(associateList);
            var reporterStrings = hubFormReference.AssociateToString(associateList);
            timePortionDateTimePickerSubmitError.Format = DateTimePickerFormat.Time;
            timePortionDateTimePickerSubmitError.ShowUpDown = true;
            reporterComboBoxSubmitErrorForm.DataSource = reporterStrings;
            associateComboBoxSubmitErrorForm.DataSource = associateStrings;
            procedureComboBoxSubmitErrorForm.DataSource = hubFormReference.LoadErrorTitles();
            severityComboBoxSubmitError.DataSource = severityList;
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void SubmitButtonSubmitErrorForm_Click(object sender, EventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedErrorsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Errors");
            ErrorsModel newError = new ErrorsModel();
            newError.AssociateReporting = reporterComboBoxSubmitErrorForm.SelectedItem.ToString();
            newError.AssociateResponsible = associateComboBoxSubmitErrorForm.SelectedItem.ToString();
            DateTime fullDateTime = datePortionDateTimePicker1.Value.Date + timePortionDateTimePickerSubmitError.Value.TimeOfDay;
            newError.ErrorDate = fullDateTime;
            newError.ErrorDescription = richTextBox1.Text;
            newError.ProcedureError = procedureComboBoxSubmitErrorForm.SelectedItem.ToString();
            newError.Severity = severityComboBoxSubmitError.SelectedItem.ToString();
            string errorDateTime = fullDateTime.ToString("yyyy-MM-dd-HH-mm-ss");
            string filePath = Path.Combine(extendedErrorsPath, errorDateTime);
            if (!File.Exists(filePath))
            {
                using (StreamWriter sw = File.CreateText(filePath))
                {
                    sw.WriteLine(newError.AssociateResponsible);
                    sw.WriteLine(newError.ErrorDate);
                    sw.WriteLine(newError.Severity);
                    sw.WriteLine(newError.ProcedureError);
                    sw.WriteLine(newError.ErrorDescription);
                    sw.WriteLine(newError.AssociateReporting);

                }
            }
            richTextBox1.Clear();

        }

        private void SubmitErrorForm_Load(object sender, EventArgs e)
        {

        }
    }
}
