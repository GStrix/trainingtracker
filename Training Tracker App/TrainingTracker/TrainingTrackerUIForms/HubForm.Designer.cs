﻿namespace TrainingTrackerUIForms
{
    partial class HubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HubForm));
            this.buildingLabelMainViewer = new System.Windows.Forms.Label();
            this.buildingComboBoxMainViewer = new System.Windows.Forms.ComboBox();
            this.dataGridViewMainViewer = new System.Windows.Forms.DataGridView();
            this.employeesButtonHub = new System.Windows.Forms.Button();
            this.specsButtonHub = new System.Windows.Forms.Button();
            this.proceduresButtonHub = new System.Windows.Forms.Button();
            this.errorsButtonHub = new System.Windows.Forms.Button();
            this.trainingButtonHubForm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMainViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // buildingLabelMainViewer
            // 
            this.buildingLabelMainViewer.AutoSize = true;
            this.buildingLabelMainViewer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buildingLabelMainViewer.Location = new System.Drawing.Point(30, 19);
            this.buildingLabelMainViewer.Name = "buildingLabelMainViewer";
            this.buildingLabelMainViewer.Size = new System.Drawing.Size(103, 32);
            this.buildingLabelMainViewer.TabIndex = 0;
            this.buildingLabelMainViewer.Text = "Building";
            // 
            // buildingComboBoxMainViewer
            // 
            this.buildingComboBoxMainViewer.FormattingEnabled = true;
            this.buildingComboBoxMainViewer.Location = new System.Drawing.Point(139, 19);
            this.buildingComboBoxMainViewer.Name = "buildingComboBoxMainViewer";
            this.buildingComboBoxMainViewer.Size = new System.Drawing.Size(200, 39);
            this.buildingComboBoxMainViewer.TabIndex = 1;
            // 
            // dataGridViewMainViewer
            // 
            this.dataGridViewMainViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMainViewer.Location = new System.Drawing.Point(36, 94);
            this.dataGridViewMainViewer.Name = "dataGridViewMainViewer";
            this.dataGridViewMainViewer.RowHeadersWidth = 51;
            this.dataGridViewMainViewer.Size = new System.Drawing.Size(1345, 444);
            this.dataGridViewMainViewer.TabIndex = 2;
            this.dataGridViewMainViewer.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewMainViewer_CellContentClick);
            // 
            // employeesButtonHub
            // 
            this.employeesButtonHub.AccessibleName = "employeeButton";
            this.employeesButtonHub.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.employeesButtonHub.Location = new System.Drawing.Point(234, 582);
            this.employeesButtonHub.Name = "employeesButtonHub";
            this.employeesButtonHub.Size = new System.Drawing.Size(124, 74);
            this.employeesButtonHub.TabIndex = 3;
            this.employeesButtonHub.Text = "Employees";
            this.employeesButtonHub.UseVisualStyleBackColor = true;
            this.employeesButtonHub.Click += new System.EventHandler(this.EmployeesButtonHub_Click);
            // 
            // specsButtonHub
            // 
            this.specsButtonHub.Location = new System.Drawing.Point(427, 582);
            this.specsButtonHub.Name = "specsButtonHub";
            this.specsButtonHub.Size = new System.Drawing.Size(124, 74);
            this.specsButtonHub.TabIndex = 4;
            this.specsButtonHub.Text = "Specs";
            this.specsButtonHub.UseMnemonic = false;
            this.specsButtonHub.UseVisualStyleBackColor = true;
            this.specsButtonHub.Click += new System.EventHandler(this.SpecsButtonHub_Click);
            // 
            // proceduresButtonHub
            // 
            this.proceduresButtonHub.Location = new System.Drawing.Point(624, 582);
            this.proceduresButtonHub.Name = "proceduresButtonHub";
            this.proceduresButtonHub.Size = new System.Drawing.Size(141, 74);
            this.proceduresButtonHub.TabIndex = 5;
            this.proceduresButtonHub.Text = "Procedures";
            this.proceduresButtonHub.UseVisualStyleBackColor = true;
            this.proceduresButtonHub.Click += new System.EventHandler(this.ProceduresButtonHub_Click);
            // 
            // errorsButtonHub
            // 
            this.errorsButtonHub.Location = new System.Drawing.Point(823, 582);
            this.errorsButtonHub.Name = "errorsButtonHub";
            this.errorsButtonHub.Size = new System.Drawing.Size(132, 74);
            this.errorsButtonHub.TabIndex = 6;
            this.errorsButtonHub.Text = "Errors";
            this.errorsButtonHub.UseVisualStyleBackColor = true;
            this.errorsButtonHub.Click += new System.EventHandler(this.ErrorsButtonHub_Click);
            // 
            // trainingButtonHubForm
            // 
            this.trainingButtonHubForm.AccessibleName = "employeeButton";
            this.trainingButtonHubForm.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.trainingButtonHubForm.Location = new System.Drawing.Point(42, 582);
            this.trainingButtonHubForm.Name = "trainingButtonHubForm";
            this.trainingButtonHubForm.Size = new System.Drawing.Size(124, 74);
            this.trainingButtonHubForm.TabIndex = 7;
            this.trainingButtonHubForm.Text = "Training";
            this.trainingButtonHubForm.UseVisualStyleBackColor = true;
            this.trainingButtonHubForm.Click += new System.EventHandler(this.TrainingButtonHubForm_Click);
            // 
            // HubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1460, 694);
            this.Controls.Add(this.trainingButtonHubForm);
            this.Controls.Add(this.errorsButtonHub);
            this.Controls.Add(this.proceduresButtonHub);
            this.Controls.Add(this.specsButtonHub);
            this.Controls.Add(this.employeesButtonHub);
            this.Controls.Add(this.dataGridViewMainViewer);
            this.Controls.Add(this.buildingComboBoxMainViewer);
            this.Controls.Add(this.buildingLabelMainViewer);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HubForm";
            this.Text = "MainViewerForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMainViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label buildingLabelMainViewer;
        private System.Windows.Forms.ComboBox buildingComboBoxMainViewer;
        private System.Windows.Forms.DataGridView dataGridViewMainViewer;
        private System.Windows.Forms.Button employeesButtonHub;
        private System.Windows.Forms.Button specsButtonHub;
        private System.Windows.Forms.Button proceduresButtonHub;
        private System.Windows.Forms.Button errorsButtonHub;
        private System.Windows.Forms.Button trainingButtonHubForm;
    }
}