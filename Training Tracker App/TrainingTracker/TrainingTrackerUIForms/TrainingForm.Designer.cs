﻿namespace TrainingTrackerUIForms
{
    partial class TrainingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingForm));
            this.buildingComboBoxTraining = new System.Windows.Forms.ComboBox();
            this.buildingLabelTraining = new System.Windows.Forms.Label();
            this.associateLabelTraining = new System.Windows.Forms.Label();
            this.associateComboBoxTraining = new System.Windows.Forms.ComboBox();
            this.procedureComboBoxTraining = new System.Windows.Forms.ComboBox();
            this.procedureLabelTraining = new System.Windows.Forms.Label();
            this.trainerNameTextBoxTraining = new System.Windows.Forms.TextBox();
            this.trainerIDTextBoxTraining = new System.Windows.Forms.TextBox();
            this.trainerNameLabelTraining = new System.Windows.Forms.Label();
            this.trainerIDLabelTraining = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.trainingDateLabelTraining = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.backButtonTraining = new System.Windows.Forms.Button();
            this.saveButtonTraining = new System.Windows.Forms.Button();
            this.specDataGridViewTraining = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specDataGridViewTraining)).BeginInit();
            this.SuspendLayout();
            // 
            // buildingComboBoxTraining
            // 
            this.buildingComboBoxTraining.FormattingEnabled = true;
            this.buildingComboBoxTraining.Location = new System.Drawing.Point(123, 32);
            this.buildingComboBoxTraining.Name = "buildingComboBoxTraining";
            this.buildingComboBoxTraining.Size = new System.Drawing.Size(160, 34);
            this.buildingComboBoxTraining.TabIndex = 0;
            // 
            // buildingLabelTraining
            // 
            this.buildingLabelTraining.AutoSize = true;
            this.buildingLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildingLabelTraining.ForeColor = System.Drawing.Color.White;
            this.buildingLabelTraining.Location = new System.Drawing.Point(13, 33);
            this.buildingLabelTraining.Name = "buildingLabelTraining";
            this.buildingLabelTraining.Size = new System.Drawing.Size(96, 30);
            this.buildingLabelTraining.TabIndex = 1;
            this.buildingLabelTraining.Text = "Building:";
            // 
            // associateLabelTraining
            // 
            this.associateLabelTraining.AutoSize = true;
            this.associateLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.associateLabelTraining.ForeColor = System.Drawing.Color.White;
            this.associateLabelTraining.Location = new System.Drawing.Point(13, 90);
            this.associateLabelTraining.Name = "associateLabelTraining";
            this.associateLabelTraining.Size = new System.Drawing.Size(108, 30);
            this.associateLabelTraining.TabIndex = 2;
            this.associateLabelTraining.Text = "Associate:";
            // 
            // associateComboBoxTraining
            // 
            this.associateComboBoxTraining.FormattingEnabled = true;
            this.associateComboBoxTraining.Location = new System.Drawing.Point(135, 90);
            this.associateComboBoxTraining.Name = "associateComboBoxTraining";
            this.associateComboBoxTraining.Size = new System.Drawing.Size(160, 34);
            this.associateComboBoxTraining.TabIndex = 3;
            this.associateComboBoxTraining.SelectedIndexChanged += new System.EventHandler(this.AssociateComboBoxTraining_SelectedIndexChanged);
            // 
            // procedureComboBoxTraining
            // 
            this.procedureComboBoxTraining.FormattingEnabled = true;
            this.procedureComboBoxTraining.Location = new System.Drawing.Point(147, 228);
            this.procedureComboBoxTraining.Name = "procedureComboBoxTraining";
            this.procedureComboBoxTraining.Size = new System.Drawing.Size(160, 24);
            this.procedureComboBoxTraining.TabIndex = 4;
            this.procedureComboBoxTraining.SelectedIndexChanged += new System.EventHandler(this.SpecComboBoxTraining_SelectedIndexChanged);
            // 
            // procedureLabelTraining
            // 
            this.procedureLabelTraining.AutoSize = true;
            this.procedureLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.procedureLabelTraining.ForeColor = System.Drawing.Color.White;
            this.procedureLabelTraining.Location = new System.Drawing.Point(13, 221);
            this.procedureLabelTraining.Name = "procedureLabelTraining";
            this.procedureLabelTraining.Size = new System.Drawing.Size(118, 30);
            this.procedureLabelTraining.TabIndex = 5;
            this.procedureLabelTraining.Text = "Procedure:";
            this.procedureLabelTraining.Click += new System.EventHandler(this.Label1_Click);
            // 
            // trainerNameTextBoxTraining
            // 
            this.trainerNameTextBoxTraining.Location = new System.Drawing.Point(837, 68);
            this.trainerNameTextBoxTraining.Name = "trainerNameTextBoxTraining";
            this.trainerNameTextBoxTraining.Size = new System.Drawing.Size(169, 22);
            this.trainerNameTextBoxTraining.TabIndex = 6;
            // 
            // trainerIDTextBoxTraining
            // 
            this.trainerIDTextBoxTraining.Location = new System.Drawing.Point(837, 155);
            this.trainerIDTextBoxTraining.Name = "trainerIDTextBoxTraining";
            this.trainerIDTextBoxTraining.Size = new System.Drawing.Size(169, 22);
            this.trainerIDTextBoxTraining.TabIndex = 7;
            // 
            // trainerNameLabelTraining
            // 
            this.trainerNameLabelTraining.AutoSize = true;
            this.trainerNameLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trainerNameLabelTraining.ForeColor = System.Drawing.Color.White;
            this.trainerNameLabelTraining.Location = new System.Drawing.Point(831, 34);
            this.trainerNameLabelTraining.Name = "trainerNameLabelTraining";
            this.trainerNameLabelTraining.Size = new System.Drawing.Size(148, 30);
            this.trainerNameLabelTraining.TabIndex = 9;
            this.trainerNameLabelTraining.Text = "Trainer Name:";
            // 
            // trainerIDLabelTraining
            // 
            this.trainerIDLabelTraining.AutoSize = true;
            this.trainerIDLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trainerIDLabelTraining.ForeColor = System.Drawing.Color.White;
            this.trainerIDLabelTraining.Location = new System.Drawing.Point(831, 121);
            this.trainerIDLabelTraining.Name = "trainerIDLabelTraining";
            this.trainerIDLabelTraining.Size = new System.Drawing.Size(111, 30);
            this.trainerIDLabelTraining.TabIndex = 10;
            this.trainerIDLabelTraining.Text = "Trainer ID:";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(516, 46);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 12;
            // 
            // trainingDateLabelTraining
            // 
            this.trainingDateLabelTraining.AutoSize = true;
            this.trainingDateLabelTraining.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trainingDateLabelTraining.ForeColor = System.Drawing.Color.White;
            this.trainingDateLabelTraining.Location = new System.Drawing.Point(510, 12);
            this.trainingDateLabelTraining.Name = "trainingDateLabelTraining";
            this.trainingDateLabelTraining.Size = new System.Drawing.Size(145, 30);
            this.trainingDateLabelTraining.TabIndex = 13;
            this.trainingDateLabelTraining.Text = "Training Date:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.associateComboBoxTraining);
            this.groupBox1.Controls.Add(this.associateLabelTraining);
            this.groupBox1.Controls.Add(this.buildingLabelTraining);
            this.groupBox1.Controls.Add(this.buildingComboBoxTraining);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(450, 146);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Building and Employee";
            // 
            // backButtonTraining
            // 
            this.backButtonTraining.Location = new System.Drawing.Point(28, 635);
            this.backButtonTraining.Name = "backButtonTraining";
            this.backButtonTraining.Size = new System.Drawing.Size(102, 34);
            this.backButtonTraining.TabIndex = 15;
            this.backButtonTraining.Text = "Back";
            this.backButtonTraining.UseVisualStyleBackColor = true;
            this.backButtonTraining.Click += new System.EventHandler(this.BackButtonTraining_Click);
            // 
            // saveButtonTraining
            // 
            this.saveButtonTraining.Location = new System.Drawing.Point(1309, 593);
            this.saveButtonTraining.Name = "saveButtonTraining";
            this.saveButtonTraining.Size = new System.Drawing.Size(122, 76);
            this.saveButtonTraining.TabIndex = 16;
            this.saveButtonTraining.Text = "Save";
            this.saveButtonTraining.UseVisualStyleBackColor = true;
            // 
            // specDataGridViewTraining
            // 
            this.specDataGridViewTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.specDataGridViewTraining.Location = new System.Drawing.Point(18, 297);
            this.specDataGridViewTraining.Name = "specDataGridViewTraining";
            this.specDataGridViewTraining.RowHeadersWidth = 51;
            this.specDataGridViewTraining.RowTemplate.Height = 24;
            this.specDataGridViewTraining.Size = new System.Drawing.Size(1413, 278);
            this.specDataGridViewTraining.TabIndex = 17;
            // 
            // TrainingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1443, 681);
            this.Controls.Add(this.specDataGridViewTraining);
            this.Controls.Add(this.saveButtonTraining);
            this.Controls.Add(this.backButtonTraining);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.trainingDateLabelTraining);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.trainerIDLabelTraining);
            this.Controls.Add(this.trainerNameLabelTraining);
            this.Controls.Add(this.trainerIDTextBoxTraining);
            this.Controls.Add(this.trainerNameTextBoxTraining);
            this.Controls.Add(this.procedureLabelTraining);
            this.Controls.Add(this.procedureComboBoxTraining);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TrainingForm";
            this.Text = "TrainingForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specDataGridViewTraining)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox buildingComboBoxTraining;
        private System.Windows.Forms.Label buildingLabelTraining;
        private System.Windows.Forms.Label associateLabelTraining;
        private System.Windows.Forms.ComboBox associateComboBoxTraining;
        private System.Windows.Forms.ComboBox procedureComboBoxTraining;
        private System.Windows.Forms.Label procedureLabelTraining;
        private System.Windows.Forms.TextBox trainerNameTextBoxTraining;
        private System.Windows.Forms.TextBox trainerIDTextBoxTraining;
        private System.Windows.Forms.Label trainerNameLabelTraining;
        private System.Windows.Forms.Label trainerIDLabelTraining;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label trainingDateLabelTraining;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button backButtonTraining;
        private System.Windows.Forms.Button saveButtonTraining;
        private System.Windows.Forms.DataGridView specDataGridViewTraining;
    }
}