﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainingTrackerUIForms
{
    public partial class DeleteAssociateForm : Form
    {
        public bool confirmDelete { get; set; }

        public DeleteAssociateForm()
        {
            InitializeComponent();
            
        }

        private void DeleteAssociateButtonDeleteAssociate_Click(object sender, EventArgs e)
        {
            if (deleteAssociateConfirmationCheckBoxDeleteAssociate.Checked)
            {
                this.confirmDelete = true;
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void CancelButtonDeleteAssociateForm_Click(object sender, EventArgs e)
        {
            this.confirmDelete = false;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
