﻿namespace TrainingTrackerUIForms
{
    partial class EditProcedureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditProcedureForm));
            this.procedureToChangeComboBoxEditProcedureForm = new System.Windows.Forms.ComboBox();
            this.procedureToChangeLabelEditProcedureForm = new System.Windows.Forms.Label();
            this.newProcedureComboBoxEditProcedureForm = new System.Windows.Forms.ComboBox();
            this.currentSpecsDataGridViewEditProcedure = new System.Windows.Forms.DataGridView();
            this.selectedSpecLabelEditProcedure = new System.Windows.Forms.Label();
            this.removeSpecButtonEditProcedure = new System.Windows.Forms.Button();
            this.backButtonEditProcedure = new System.Windows.Forms.Button();
            this.newProcedureButtonEditProcedure = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.currentSpecsDataGridViewEditProcedure)).BeginInit();
            this.SuspendLayout();
            // 
            // procedureToChangeComboBoxEditProcedureForm
            // 
            this.procedureToChangeComboBoxEditProcedureForm.FormattingEnabled = true;
            this.procedureToChangeComboBoxEditProcedureForm.Location = new System.Drawing.Point(294, 29);
            this.procedureToChangeComboBoxEditProcedureForm.Name = "procedureToChangeComboBoxEditProcedureForm";
            this.procedureToChangeComboBoxEditProcedureForm.Size = new System.Drawing.Size(296, 39);
            this.procedureToChangeComboBoxEditProcedureForm.TabIndex = 0;
            this.procedureToChangeComboBoxEditProcedureForm.SelectedIndexChanged += new System.EventHandler(this.ProcedureToChangeComboBoxEditProcedureForm_SelectedIndexChanged);
            // 
            // procedureToChangeLabelEditProcedureForm
            // 
            this.procedureToChangeLabelEditProcedureForm.AutoSize = true;
            this.procedureToChangeLabelEditProcedureForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.procedureToChangeLabelEditProcedureForm.Location = new System.Drawing.Point(37, 32);
            this.procedureToChangeLabelEditProcedureForm.Name = "procedureToChangeLabelEditProcedureForm";
            this.procedureToChangeLabelEditProcedureForm.Size = new System.Drawing.Size(249, 32);
            this.procedureToChangeLabelEditProcedureForm.TabIndex = 1;
            this.procedureToChangeLabelEditProcedureForm.Text = "Procedure To Change:";
            // 
            // newProcedureComboBoxEditProcedureForm
            // 
            this.newProcedureComboBoxEditProcedureForm.FormattingEnabled = true;
            this.newProcedureComboBoxEditProcedureForm.Location = new System.Drawing.Point(43, 170);
            this.newProcedureComboBoxEditProcedureForm.Name = "newProcedureComboBoxEditProcedureForm";
            this.newProcedureComboBoxEditProcedureForm.Size = new System.Drawing.Size(296, 39);
            this.newProcedureComboBoxEditProcedureForm.TabIndex = 3;
            // 
            // currentSpecsDataGridViewEditProcedure
            // 
            this.currentSpecsDataGridViewEditProcedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.currentSpecsDataGridViewEditProcedure.Location = new System.Drawing.Point(18, 233);
            this.currentSpecsDataGridViewEditProcedure.Name = "currentSpecsDataGridViewEditProcedure";
            this.currentSpecsDataGridViewEditProcedure.RowHeadersWidth = 51;
            this.currentSpecsDataGridViewEditProcedure.RowTemplate.Height = 24;
            this.currentSpecsDataGridViewEditProcedure.Size = new System.Drawing.Size(1046, 390);
            this.currentSpecsDataGridViewEditProcedure.TabIndex = 17;
            this.currentSpecsDataGridViewEditProcedure.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CurrentSpecsDataGridViewEditProcedure_CellContentClick);
            // 
            // selectedSpecLabelEditProcedure
            // 
            this.selectedSpecLabelEditProcedure.AutoSize = true;
            this.selectedSpecLabelEditProcedure.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectedSpecLabelEditProcedure.Location = new System.Drawing.Point(350, 170);
            this.selectedSpecLabelEditProcedure.Name = "selectedSpecLabelEditProcedure";
            this.selectedSpecLabelEditProcedure.Size = new System.Drawing.Size(267, 32);
            this.selectedSpecLabelEditProcedure.TabIndex = 18;
            this.selectedSpecLabelEditProcedure.Text = "Spec to Add or Remove";
            // 
            // removeSpecButtonEditProcedure
            // 
            this.removeSpecButtonEditProcedure.Location = new System.Drawing.Point(782, 125);
            this.removeSpecButtonEditProcedure.Name = "removeSpecButtonEditProcedure";
            this.removeSpecButtonEditProcedure.Size = new System.Drawing.Size(169, 84);
            this.removeSpecButtonEditProcedure.TabIndex = 20;
            this.removeSpecButtonEditProcedure.Text = "Delete Procedure";
            this.removeSpecButtonEditProcedure.UseVisualStyleBackColor = true;
            this.removeSpecButtonEditProcedure.Click += new System.EventHandler(this.RemoveSpecButtonEditProcedure_Click);
            // 
            // backButtonEditProcedure
            // 
            this.backButtonEditProcedure.Location = new System.Drawing.Point(18, 642);
            this.backButtonEditProcedure.Name = "backButtonEditProcedure";
            this.backButtonEditProcedure.Size = new System.Drawing.Size(143, 46);
            this.backButtonEditProcedure.TabIndex = 21;
            this.backButtonEditProcedure.Text = "Back";
            this.backButtonEditProcedure.UseVisualStyleBackColor = true;
            this.backButtonEditProcedure.Click += new System.EventHandler(this.BackButtonEditProcedure_Click);
            // 
            // newProcedureButtonEditProcedure
            // 
            this.newProcedureButtonEditProcedure.Location = new System.Drawing.Point(782, 12);
            this.newProcedureButtonEditProcedure.Name = "newProcedureButtonEditProcedure";
            this.newProcedureButtonEditProcedure.Size = new System.Drawing.Size(169, 84);
            this.newProcedureButtonEditProcedure.TabIndex = 22;
            this.newProcedureButtonEditProcedure.Text = "New Procedure";
            this.newProcedureButtonEditProcedure.UseVisualStyleBackColor = true;
            this.newProcedureButtonEditProcedure.Click += new System.EventHandler(this.NewProcedureButtonEditProcedure_Click);
            // 
            // EditProcedureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1092, 700);
            this.Controls.Add(this.newProcedureButtonEditProcedure);
            this.Controls.Add(this.backButtonEditProcedure);
            this.Controls.Add(this.removeSpecButtonEditProcedure);
            this.Controls.Add(this.selectedSpecLabelEditProcedure);
            this.Controls.Add(this.currentSpecsDataGridViewEditProcedure);
            this.Controls.Add(this.newProcedureComboBoxEditProcedureForm);
            this.Controls.Add(this.procedureToChangeLabelEditProcedureForm);
            this.Controls.Add(this.procedureToChangeComboBoxEditProcedureForm);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "EditProcedureForm";
            this.Text = "EditProcedureForm";
            ((System.ComponentModel.ISupportInitialize)(this.currentSpecsDataGridViewEditProcedure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox procedureToChangeComboBoxEditProcedureForm;
        private System.Windows.Forms.Label procedureToChangeLabelEditProcedureForm;
        private System.Windows.Forms.ComboBox newProcedureComboBoxEditProcedureForm;
        private System.Windows.Forms.DataGridView currentSpecsDataGridViewEditProcedure;
        private System.Windows.Forms.Label selectedSpecLabelEditProcedure;
        private System.Windows.Forms.Button removeSpecButtonEditProcedure;
        private System.Windows.Forms.Button backButtonEditProcedure;
        private System.Windows.Forms.Button newProcedureButtonEditProcedure;
    }
}