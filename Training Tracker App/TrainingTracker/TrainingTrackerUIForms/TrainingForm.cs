﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.IO;
using System.Diagnostics;

namespace TrainingTrackerUIForms
{
    public partial class TrainingForm : Form
    {
        public TrainingForm()
        {
            InitializeComponent();
            var editProcedureFormRef = new EditProcedureForm();
            var hubFormReference = new HubForm();
            List<string> procedureList = new List<string>();
            List<string> associateList = new List<string>();
            

            DataTable dt = new DataTable();
            

            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.HeaderText = "Select";
            select.DataPropertyName = "Select";

            DataGridViewTextBoxColumn specName = new DataGridViewTextBoxColumn();
            specName.HeaderText = "Spec Name";
            specName.DataPropertyName = "Spec Name";

            DataGridViewTextBoxColumn specCompletionDate = new DataGridViewTextBoxColumn();
            specCompletionDate.HeaderText = "Spec Completion Date";
            specCompletionDate.DataPropertyName = "Spec Completion Date";

            DataGridViewTextBoxColumn documentRevision = new DataGridViewTextBoxColumn();
            documentRevision.HeaderText = "Document Revision";
            documentRevision.DataPropertyName = "Document Revision";

            specDataGridViewTraining.DataSource = dt;
            specDataGridViewTraining.Columns.AddRange(select);
            specDataGridViewTraining.Columns.AddRange(specName);
            specDataGridViewTraining.Columns.AddRange(specCompletionDate);
            specDataGridViewTraining.Columns.AddRange(documentRevision);

            specDataGridViewTraining.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            procedureComboBoxTraining.DataSource = editProcedureFormRef.LoadProcedures();
            associateComboBoxTraining.DataSource = hubFormReference.AssociateToString(hubFormReference.LoadAssociates());
            procedureComboBoxTraining.SelectedItem = null;
            associateComboBoxTraining.SelectedItem = null;
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void BackButtonTraining_Click(object sender, EventArgs e)
        {
            var hubForm = new HubForm();
            hubForm.Show();
            this.Close();
        }

        private void SpecComboBoxTraining_SelectedIndexChanged(object sender, EventArgs e)
        {
            var editProcedureFormRef = new EditProcedureForm();
            var hubFormRef = new HubForm();
            string asscName = associateComboBoxTraining.Text;
            List<SpecModel> asscSpecs = new List<SpecModel>();
            List<string> asscNames = new List<string>();
            asscNames.Add(asscName);
            List<AssociateModel> asscModel = hubFormRef.StringToAssociate(asscNames);

            if ((associateComboBoxTraining != null && procedureComboBoxTraining != null) && associateComboBoxTraining.Text != "")
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
                string filePath = Path.Combine(extendedAssociatePath + "\\", asscName + "\\Specs");
                if (Directory.Exists(filePath))
                {
                    string[] files = Directory.GetFiles(extendedAssociatePath + "\\" + asscName + "\\Specs");
                    string procedureName = procedureComboBoxTraining.Text;
                    Debug.WriteLine("Procedure name from procedure combobox change");
                    Debug.WriteLine(procedureName);
                    List<String> specsAssociatedToProcedure = editProcedureFormRef.LoadSpecsAssociatedToProcedure(procedureName);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Select", typeof(bool));
                    dt.Columns.Add("Spec Name", typeof(string));
                    dt.Columns.Add("Spec Completion Date", typeof(string));
                    dt.Columns.Add("Document Revision", typeof(string));
                    dt = FillProcedureSpecDataTable(asscModel.First(), dt, specsAssociatedToProcedure); ;
                    specDataGridViewTraining.DataSource = dt;
                }
                
            }
            
        }

        private void AssociateComboBoxTraining_SelectedIndexChanged(object sender, EventArgs e)
        {
            var editProcedureFormRef = new EditProcedureForm();
            var hubFormRef = new HubForm();
            string asscName = associateComboBoxTraining.Text;
            List<SpecModel> asscSpecs = new List<SpecModel>();
            List<string> asscNames = new List<string>();
            asscNames.Add(asscName);
            List<AssociateModel> asscModel = hubFormRef.StringToAssociate(asscNames);

            if ((associateComboBoxTraining != null && procedureComboBoxTraining != null) || associateComboBoxTraining.Text != "")
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
                string filePath = extendedAssociatePath + "\\" + asscName + "\\Specs";
                if (Directory.Exists(filePath))
                {
                    string[] files = Directory.GetFiles(extendedAssociatePath + "\\" + asscName + "\\Specs");
                    string procedureName = procedureComboBoxTraining.Text;
                    Debug.WriteLine("Procedure name from associate combobox change");
                    Debug.WriteLine(procedureName);
                    List<String> specsAssociatedToProcedure = editProcedureFormRef.LoadSpecsAssociatedToProcedure(procedureName);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Select", typeof(bool));
                    dt.Columns.Add("Spec Name", typeof(string));
                    dt.Columns.Add("Spec Completion Date", typeof(string));
                    dt.Columns.Add("Document Revision", typeof(string));
                    dt = FillProcedureSpecDataTable(asscModel.First(), dt, specsAssociatedToProcedure);
                    specDataGridViewTraining.DataSource = dt;
                }
                
            }

        }

        private DataTable FillProcedureSpecDataTable(AssociateModel assc, DataTable table, List<String> specsAssociatedToProcedure)
        {
            
            foreach (SpecModel spec in assc.Specs) if (specsAssociatedToProcedure.Contains(spec.SpecName))
            {
                DataRow dataRow = null;
                dataRow = table.NewRow();
                dataRow["Select"] = false;
                dataRow["Spec Name"] = spec.SpecName;
                dataRow["Spec Completion Date"] = spec.SpecCompletionDate;
                dataRow["Document Revision"] = spec.RevisionVersion;
                table.Rows.Add(dataRow);
            }
            return table;
        }
    }
}
