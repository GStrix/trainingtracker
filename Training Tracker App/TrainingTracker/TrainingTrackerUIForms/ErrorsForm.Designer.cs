﻿namespace TrainingTrackerUIForms
{
    partial class ErrorsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorsForm));
            this.buildingLabelErrorsForm = new System.Windows.Forms.Label();
            this.buildingComboBoxErrorsForm = new System.Windows.Forms.ComboBox();
            this.associateLabelErrorsForm = new System.Windows.Forms.Label();
            this.associateComboBoxErrorsForm = new System.Windows.Forms.ComboBox();
            this.procedureLabelErrorsForm = new System.Windows.Forms.Label();
            this.procedureComboBoxErrorsForm = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sendReportButtonErrorsForm = new System.Windows.Forms.Button();
            this.submitNewErrorButtonErrorsForm = new System.Windows.Forms.Button();
            this.backButtonErrors = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buildingLabelErrorsForm
            // 
            this.buildingLabelErrorsForm.AutoSize = true;
            this.buildingLabelErrorsForm.Location = new System.Drawing.Point(25, 25);
            this.buildingLabelErrorsForm.Name = "buildingLabelErrorsForm";
            this.buildingLabelErrorsForm.Size = new System.Drawing.Size(108, 32);
            this.buildingLabelErrorsForm.TabIndex = 0;
            this.buildingLabelErrorsForm.Text = "Building:";
            // 
            // buildingComboBoxErrorsForm
            // 
            this.buildingComboBoxErrorsForm.FormattingEnabled = true;
            this.buildingComboBoxErrorsForm.Location = new System.Drawing.Point(139, 22);
            this.buildingComboBoxErrorsForm.Name = "buildingComboBoxErrorsForm";
            this.buildingComboBoxErrorsForm.Size = new System.Drawing.Size(215, 39);
            this.buildingComboBoxErrorsForm.TabIndex = 1;
            // 
            // associateLabelErrorsForm
            // 
            this.associateLabelErrorsForm.AutoSize = true;
            this.associateLabelErrorsForm.Location = new System.Drawing.Point(25, 106);
            this.associateLabelErrorsForm.Name = "associateLabelErrorsForm";
            this.associateLabelErrorsForm.Size = new System.Drawing.Size(119, 32);
            this.associateLabelErrorsForm.TabIndex = 2;
            this.associateLabelErrorsForm.Text = "Associate:";
            // 
            // associateComboBoxErrorsForm
            // 
            this.associateComboBoxErrorsForm.FormattingEnabled = true;
            this.associateComboBoxErrorsForm.Location = new System.Drawing.Point(150, 103);
            this.associateComboBoxErrorsForm.Name = "associateComboBoxErrorsForm";
            this.associateComboBoxErrorsForm.Size = new System.Drawing.Size(221, 39);
            this.associateComboBoxErrorsForm.TabIndex = 3;
            this.associateComboBoxErrorsForm.SelectedIndexChanged += new System.EventHandler(this.AssociateComboBoxErrorsForm_SelectedIndexChanged);
            // 
            // procedureLabelErrorsForm
            // 
            this.procedureLabelErrorsForm.AutoSize = true;
            this.procedureLabelErrorsForm.Location = new System.Drawing.Point(25, 185);
            this.procedureLabelErrorsForm.Name = "procedureLabelErrorsForm";
            this.procedureLabelErrorsForm.Size = new System.Drawing.Size(135, 32);
            this.procedureLabelErrorsForm.TabIndex = 4;
            this.procedureLabelErrorsForm.Text = "Procedure: ";
            // 
            // procedureComboBoxErrorsForm
            // 
            this.procedureComboBoxErrorsForm.FormattingEnabled = true;
            this.procedureComboBoxErrorsForm.Location = new System.Drawing.Point(166, 182);
            this.procedureComboBoxErrorsForm.Name = "procedureComboBoxErrorsForm";
            this.procedureComboBoxErrorsForm.Size = new System.Drawing.Size(233, 39);
            this.procedureComboBoxErrorsForm.TabIndex = 5;
            this.procedureComboBoxErrorsForm.SelectedIndexChanged += new System.EventHandler(this.ProcedureComboBoxErrorsForm_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(31, 278);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1467, 335);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // sendReportButtonErrorsForm
            // 
            this.sendReportButtonErrorsForm.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.sendReportButtonErrorsForm.Location = new System.Drawing.Point(579, 619);
            this.sendReportButtonErrorsForm.Name = "sendReportButtonErrorsForm";
            this.sendReportButtonErrorsForm.Size = new System.Drawing.Size(155, 77);
            this.sendReportButtonErrorsForm.TabIndex = 7;
            this.sendReportButtonErrorsForm.Text = "Send Report";
            this.sendReportButtonErrorsForm.UseVisualStyleBackColor = true;
            this.sendReportButtonErrorsForm.Click += new System.EventHandler(this.SendReportButtonErrorsForm_Click);
            // 
            // submitNewErrorButtonErrorsForm
            // 
            this.submitNewErrorButtonErrorsForm.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.submitNewErrorButtonErrorsForm.Location = new System.Drawing.Point(820, 619);
            this.submitNewErrorButtonErrorsForm.Name = "submitNewErrorButtonErrorsForm";
            this.submitNewErrorButtonErrorsForm.Size = new System.Drawing.Size(155, 77);
            this.submitNewErrorButtonErrorsForm.TabIndex = 8;
            this.submitNewErrorButtonErrorsForm.Text = "Submit New Error";
            this.submitNewErrorButtonErrorsForm.UseVisualStyleBackColor = true;
            this.submitNewErrorButtonErrorsForm.Click += new System.EventHandler(this.SubmitNewErrorButtonErrorsForm_Click);
            // 
            // backButtonErrors
            // 
            this.backButtonErrors.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.backButtonErrors.Location = new System.Drawing.Point(12, 736);
            this.backButtonErrors.Name = "backButtonErrors";
            this.backButtonErrors.Size = new System.Drawing.Size(121, 51);
            this.backButtonErrors.TabIndex = 9;
            this.backButtonErrors.Text = "Back";
            this.backButtonErrors.UseVisualStyleBackColor = true;
            this.backButtonErrors.Click += new System.EventHandler(this.BackButtonErrors_Click);
            // 
            // ErrorsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1514, 799);
            this.Controls.Add(this.backButtonErrors);
            this.Controls.Add(this.submitNewErrorButtonErrorsForm);
            this.Controls.Add(this.sendReportButtonErrorsForm);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.procedureComboBoxErrorsForm);
            this.Controls.Add(this.procedureLabelErrorsForm);
            this.Controls.Add(this.associateComboBoxErrorsForm);
            this.Controls.Add(this.associateLabelErrorsForm);
            this.Controls.Add(this.buildingComboBoxErrorsForm);
            this.Controls.Add(this.buildingLabelErrorsForm);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "ErrorsForm";
            this.Text = "ErrorsForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label buildingLabelErrorsForm;
        private System.Windows.Forms.ComboBox buildingComboBoxErrorsForm;
        private System.Windows.Forms.Label associateLabelErrorsForm;
        private System.Windows.Forms.ComboBox associateComboBoxErrorsForm;
        private System.Windows.Forms.Label procedureLabelErrorsForm;
        private System.Windows.Forms.ComboBox procedureComboBoxErrorsForm;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button sendReportButtonErrorsForm;
        private System.Windows.Forms.Button submitNewErrorButtonErrorsForm;
        private System.Windows.Forms.Button backButtonErrors;
    }
}