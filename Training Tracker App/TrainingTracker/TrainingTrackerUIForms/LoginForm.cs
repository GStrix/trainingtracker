﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;

namespace TrainingTrackerUIForms
{
    public partial class LoginForm : Form
    {
        public static LoginForm loginFormInstance;
        public LoginForm()
        {
            loginFormInstance = this;
            InitializeComponent();
            
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            var hubFormReference = new HubForm();
            List<AssociateModel> asscs = hubFormReference.LoadAssociates();
            string enteredPassword = passwordTextBox.Text;
            string enteredEmail = usernameTextBox.Text;
            foreach (var assc in asscs)
            {
                if (assc.EmailAddress == enteredEmail && assc.Password == enteredPassword && assc.AdminAccess)
                {
                    
                    WindowState = FormWindowState.Minimized;
                    ShowInTaskbar = true;
                    Visible = true;
                    var HubForm = new HubForm();
                    {
                        TopMost = true;
                    }
                    HubForm.Show();
                    HubForm.TopMost = false;
                    //closes whole program: this.Close();
                    return;
                }
            }
            
        }
    }
}
