﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.IO;

namespace TrainingTrackerUIForms
{
    public partial class AddNewAssociateForm : Form
    {
        public AddNewAssociateForm()
        {
            InitializeComponent();
            var hubFormReference = new HubForm();
            supervisorComboBoxAddNewAssociate.DataSource = hubFormReference.LoadSupervisors();
            jobTitleComboBoxAddNewAssociate.DataSource = hubFormReference.LoadJobTitles();
        }

        private void AddAssociateButtonAddNewAssociate_Click(object sender, EventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            if (ValidateForm())
            {
                AssociateModel model = new AssociateModel();

                model.FirstName = firstNameTextBoxAddNewAssociate.Text;
                model.LastName = lastNameTextBoxAddNewAssociate.Text;
                model.Password = passwordTextBoxAddNewAssociate.Text;
                model.StartDate = monthCalendar1.SelectionRange.Start.ToShortDateString();
                model.EmailAddress = emailAddressTextBoxAddNewAssociate.Text;
                model.AdminAccess = adminCheckBoxAddNewAssociate.Checked;
                model.SupervisorStatus = supervisorCheckBoxAddNewAssociate.Checked;
                model.employeeId = employeeIDTextBoxAddNewAssociate.Text;
                model.supervisor = supervisorComboBoxAddNewAssociate.Text;
                model.jobTitle = jobTitleComboBoxAddNewAssociate.Text;
                //model.AssociateErrors = new List<ErrorsModel>();
                //model.Procedures = new List<JobPositionModel>();
                var specUpdateFormRef = new SpecUpdateForm();
                List<string> specStrings = specUpdateFormRef.LoadSpecs();
                string folderName = model.FirstName + " " + model.LastName;
                string filePath = Path.Combine(extendedAssociatePath, folderName + "\\Data.txt");
                string specFilePath = Path.Combine(extendedAssociatePath, folderName + "\\Specs");
                Directory.CreateDirectory(Path.Combine(extendedAssociatePath, folderName +"\\Specs"));
                Directory.CreateDirectory(Path.Combine(extendedAssociatePath, folderName));
                if (!File.Exists(filePath))
                {
                    using(StreamWriter sw = File.CreateText(filePath))
                    {
                        sw.WriteLine(model.FirstName);
                        sw.WriteLine(model.LastName);
                        sw.WriteLine(model.employeeId);
                        sw.WriteLine(model.EmailAddress);
                        sw.WriteLine(model.StartDate);
                        sw.WriteLine(model.supervisor);
                        if (model.SupervisorStatus)
                        {
                            sw.WriteLine("Y");
                        }
                        else
                            sw.WriteLine("N");
                        if (model.AdminAccess)
                        {
                            sw.WriteLine("Y");
                        }
                        else
                            sw.WriteLine("N");
                        sw.WriteLine(model.Password);
                        sw.WriteLine(model.jobTitle);
                        
                    }
                }
                foreach (string spec in specStrings)
                {
                    string textFilePath = specFilePath + "\\" + spec + ".txt"; 
                    using (StreamWriter sw = File.CreateText(textFilePath))
                    {
                        sw.WriteLine("N/A");
                        sw.WriteLine("N/A");
                    }
                }
            }
            firstNameTextBoxAddNewAssociate.Clear();
            lastNameTextBoxAddNewAssociate.Clear();
            passwordTextBoxAddNewAssociate.Clear();
            emailAddressTextBoxAddNewAssociate.Clear();
            employeeIDTextBoxAddNewAssociate.Clear();
        }

        private bool ValidateForm()
        {
            bool output = true;
            int date = 0;
            bool DateInputted;


            if (firstNameTextBoxAddNewAssociate.Text.Length == 0)
            {
                output = false;
            }

            if (monthCalendar1.SelectionRange.Start.ToShortDateString().Length < 2)
            {
                output = false;
            }

            if (emailAddressTextBoxAddNewAssociate.Text.Length == 0 || !emailAddressTextBoxAddNewAssociate.Text.Contains("@rocelec.com"))
            {
                output = false;
            }

            if (passwordTextBoxAddNewAssociate.Text.Length <= 3)
            {
                output = false;
            }

            return output;
        }

        private void JobTitleComboBoxAddNewAssociate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BackButtonAddNewAssociate_Click(object sender, EventArgs e)
        {
            var editAssociateForm = new EditAssociateForm();
            editAssociateForm.Show();
            this.Close();
        }
    }
}
