﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.IO;

namespace TrainingTrackerUIForms
{
    public partial class SpecUpdateForm : Form
    {
        public SpecUpdateForm()
        {
            InitializeComponent();
            comboBox1.DataSource = LoadSpecs();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(SpecUpdateForm_DragEnter);
            this.DragDrop += new DragEventHandler(SpecUpdateForm_DragDrop);

        }

        private void SpecUpdateForm_DragEnter(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null)
            {
                e.Effect = DragDropEffects.Copy;
            }

        }

        private void SpecUpdateForm_DragDrop(object sender, DragEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedSpecsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Specs");
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null)
            {
                foreach (string file in files)
                {
                    string fileName = Path.GetFileName(file);
                    string destinationName = Path.Combine(extendedSpecsPath + "\\", fileName);
                    File.Move(file, destinationName);
                }
            }
            comboBox1.DataSource = LoadSpecs();
        }

        private void BackButtonSpecUpdate_Click(object sender, EventArgs e)
        {
            var hubForm = new HubForm();
            hubForm.Show();
            this.Close();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public List<String> LoadSpecs()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedSpecsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Specs");

            string[] fileArray = Directory.GetFiles(extendedSpecsPath);
            List<String> fileNames = new List<string>();
            foreach(var file in fileArray)
            {  
                fileNames.Add(Path.GetFileNameWithoutExtension(file));
            }
                
            return fileNames.ToList<String>();
        }

        private void DeleteSpecButtonSpecUpdate_Click(object sender, EventArgs e)
        {
            var deleteSpecForm = new DeleteSpecForm();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedSpecsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Specs");
            string fileName = comboBox1.SelectedItem.ToString();
            string destinationName = Path.Combine(extendedSpecsPath + "\\", fileName);
            var result = deleteSpecForm.ShowDialog();
            if (result == DialogResult.Yes)
            {
                File.Delete(destinationName);
                var editAssociateFormRef = new EditAssociateForm();
                var hubFormReference = new HubForm();
                editAssociateFormRef.UpdateAssociateSpecs(LoadSpecs(), hubFormReference.LoadAssociates());
                comboBox1.DataSource = LoadSpecs();
            }
        }
    }
}
