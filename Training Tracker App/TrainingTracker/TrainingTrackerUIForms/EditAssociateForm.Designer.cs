﻿namespace TrainingTrackerUIForms
{
    partial class EditAssociateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditAssociateForm));
            this.backButtonEditAssociate = new System.Windows.Forms.Button();
            this.supervisorComboBoxEditAssociate = new System.Windows.Forms.ComboBox();
            this.supervisorLabelEditAssociate = new System.Windows.Forms.Label();
            this.associateLabelEditAssociate = new System.Windows.Forms.Label();
            this.associateComboBoxEditAssociate = new System.Windows.Forms.ComboBox();
            this.saveButtonEditAssociate = new System.Windows.Forms.Button();
            this.employeeStartDateLabelEditAssociate = new System.Windows.Forms.Label();
            this.dateOfTrainingCalendarEditAssociate = new System.Windows.Forms.MonthCalendar();
            this.firstNameTextBoxEditAssociate = new System.Windows.Forms.TextBox();
            this.associateFirstNameLabelEditAssociate = new System.Windows.Forms.Label();
            this.employeeIDtextBoxEditAssociate = new System.Windows.Forms.TextBox();
            this.employeeIDLabelEditAssociate = new System.Windows.Forms.Label();
            this.jobTitlelabelEditAssociate = new System.Windows.Forms.Label();
            this.jobTitlecomboBoxEditAssociate = new System.Windows.Forms.ComboBox();
            this.associateBuildingComboBoxEditAssociate = new System.Windows.Forms.ComboBox();
            this.associateBuildingLabelEditAssociate = new System.Windows.Forms.Label();
            this.deleteAssociateButtonEditAssociate = new System.Windows.Forms.Button();
            this.employeeEmailTextBoxEditAssociate = new System.Windows.Forms.TextBox();
            this.employeeEmailLabelEditAssociate = new System.Windows.Forms.Label();
            this.NewAssociateButtonEditAssociate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.supervisorCheckBoxEditAssociate = new System.Windows.Forms.CheckBox();
            this.reportsToComboBoxEditAssociate = new System.Windows.Forms.ComboBox();
            this.reportsToLabelEditAssociate = new System.Windows.Forms.Label();
            this.associateLastNameLabelEditAssociateForm = new System.Windows.Forms.Label();
            this.lastNameTextBoxEditAssociateForm = new System.Windows.Forms.TextBox();
            this.adminCheckBoxEditAssociate = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backButtonEditAssociate
            // 
            this.backButtonEditAssociate.Location = new System.Drawing.Point(12, 773);
            this.backButtonEditAssociate.Name = "backButtonEditAssociate";
            this.backButtonEditAssociate.Size = new System.Drawing.Size(108, 45);
            this.backButtonEditAssociate.TabIndex = 22;
            this.backButtonEditAssociate.Text = "Back";
            this.backButtonEditAssociate.UseVisualStyleBackColor = true;
            this.backButtonEditAssociate.Click += new System.EventHandler(this.BackButtonEditAssociate_Click);
            // 
            // supervisorComboBoxEditAssociate
            // 
            this.supervisorComboBoxEditAssociate.FormattingEnabled = true;
            this.supervisorComboBoxEditAssociate.Location = new System.Drawing.Point(149, 37);
            this.supervisorComboBoxEditAssociate.Name = "supervisorComboBoxEditAssociate";
            this.supervisorComboBoxEditAssociate.Size = new System.Drawing.Size(255, 39);
            this.supervisorComboBoxEditAssociate.TabIndex = 0;
            this.supervisorComboBoxEditAssociate.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // supervisorLabelEditAssociate
            // 
            this.supervisorLabelEditAssociate.AutoSize = true;
            this.supervisorLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.supervisorLabelEditAssociate.Location = new System.Drawing.Point(11, 40);
            this.supervisorLabelEditAssociate.Name = "supervisorLabelEditAssociate";
            this.supervisorLabelEditAssociate.Size = new System.Drawing.Size(132, 32);
            this.supervisorLabelEditAssociate.TabIndex = 1;
            this.supervisorLabelEditAssociate.Text = "Supervisor:";
            // 
            // associateLabelEditAssociate
            // 
            this.associateLabelEditAssociate.AutoSize = true;
            this.associateLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.associateLabelEditAssociate.Location = new System.Drawing.Point(11, 129);
            this.associateLabelEditAssociate.Name = "associateLabelEditAssociate";
            this.associateLabelEditAssociate.Size = new System.Drawing.Size(119, 32);
            this.associateLabelEditAssociate.TabIndex = 2;
            this.associateLabelEditAssociate.Text = "Associate:";
            // 
            // associateComboBoxEditAssociate
            // 
            this.associateComboBoxEditAssociate.FormattingEnabled = true;
            this.associateComboBoxEditAssociate.Location = new System.Drawing.Point(149, 126);
            this.associateComboBoxEditAssociate.Name = "associateComboBoxEditAssociate";
            this.associateComboBoxEditAssociate.Size = new System.Drawing.Size(255, 39);
            this.associateComboBoxEditAssociate.TabIndex = 3;
            this.associateComboBoxEditAssociate.SelectedIndexChanged += new System.EventHandler(this.ComboBox2_SelectedIndexChanged);
            // 
            // saveButtonEditAssociate
            // 
            this.saveButtonEditAssociate.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButtonEditAssociate.Location = new System.Drawing.Point(810, 531);
            this.saveButtonEditAssociate.Name = "saveButtonEditAssociate";
            this.saveButtonEditAssociate.Size = new System.Drawing.Size(164, 89);
            this.saveButtonEditAssociate.TabIndex = 9;
            this.saveButtonEditAssociate.Text = "Save";
            this.saveButtonEditAssociate.UseVisualStyleBackColor = true;
            this.saveButtonEditAssociate.Click += new System.EventHandler(this.SaveButtonEditAssociate_Click);
            // 
            // employeeStartDateLabelEditAssociate
            // 
            this.employeeStartDateLabelEditAssociate.AutoSize = true;
            this.employeeStartDateLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeStartDateLabelEditAssociate.Location = new System.Drawing.Point(304, 323);
            this.employeeStartDateLabelEditAssociate.Name = "employeeStartDateLabelEditAssociate";
            this.employeeStartDateLabelEditAssociate.Size = new System.Drawing.Size(226, 32);
            this.employeeStartDateLabelEditAssociate.TabIndex = 10;
            this.employeeStartDateLabelEditAssociate.Text = "Associate Start Date";
            // 
            // dateOfTrainingCalendarEditAssociate
            // 
            this.dateOfTrainingCalendarEditAssociate.Location = new System.Drawing.Point(30, 317);
            this.dateOfTrainingCalendarEditAssociate.Name = "dateOfTrainingCalendarEditAssociate";
            this.dateOfTrainingCalendarEditAssociate.TabIndex = 11;
            // 
            // firstNameTextBoxEditAssociate
            // 
            this.firstNameTextBoxEditAssociate.Location = new System.Drawing.Point(30, 197);
            this.firstNameTextBoxEditAssociate.Name = "firstNameTextBoxEditAssociate";
            this.firstNameTextBoxEditAssociate.Size = new System.Drawing.Size(287, 38);
            this.firstNameTextBoxEditAssociate.TabIndex = 12;
            // 
            // associateFirstNameLabelEditAssociate
            // 
            this.associateFirstNameLabelEditAssociate.AutoSize = true;
            this.associateFirstNameLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.associateFirstNameLabelEditAssociate.Location = new System.Drawing.Point(332, 203);
            this.associateFirstNameLabelEditAssociate.Name = "associateFirstNameLabelEditAssociate";
            this.associateFirstNameLabelEditAssociate.Size = new System.Drawing.Size(130, 32);
            this.associateFirstNameLabelEditAssociate.TabIndex = 13;
            this.associateFirstNameLabelEditAssociate.Text = "First Name";
            // 
            // employeeIDtextBoxEditAssociate
            // 
            this.employeeIDtextBoxEditAssociate.Location = new System.Drawing.Point(30, 542);
            this.employeeIDtextBoxEditAssociate.Name = "employeeIDtextBoxEditAssociate";
            this.employeeIDtextBoxEditAssociate.Size = new System.Drawing.Size(287, 38);
            this.employeeIDtextBoxEditAssociate.TabIndex = 14;
            // 
            // employeeIDLabelEditAssociate
            // 
            this.employeeIDLabelEditAssociate.AutoEllipsis = true;
            this.employeeIDLabelEditAssociate.AutoSize = true;
            this.employeeIDLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeIDLabelEditAssociate.Location = new System.Drawing.Point(345, 548);
            this.employeeIDLabelEditAssociate.Name = "employeeIDLabelEditAssociate";
            this.employeeIDLabelEditAssociate.Size = new System.Drawing.Size(150, 32);
            this.employeeIDLabelEditAssociate.TabIndex = 15;
            this.employeeIDLabelEditAssociate.Text = "Employee ID";
            // 
            // jobTitlelabelEditAssociate
            // 
            this.jobTitlelabelEditAssociate.AutoEllipsis = true;
            this.jobTitlelabelEditAssociate.AutoSize = true;
            this.jobTitlelabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.jobTitlelabelEditAssociate.Location = new System.Drawing.Point(345, 608);
            this.jobTitlelabelEditAssociate.Name = "jobTitlelabelEditAssociate";
            this.jobTitlelabelEditAssociate.Size = new System.Drawing.Size(105, 32);
            this.jobTitlelabelEditAssociate.TabIndex = 17;
            this.jobTitlelabelEditAssociate.Text = "Job Title";
            // 
            // jobTitlecomboBoxEditAssociate
            // 
            this.jobTitlecomboBoxEditAssociate.FormattingEnabled = true;
            this.jobTitlecomboBoxEditAssociate.Location = new System.Drawing.Point(30, 601);
            this.jobTitlecomboBoxEditAssociate.Name = "jobTitlecomboBoxEditAssociate";
            this.jobTitlecomboBoxEditAssociate.Size = new System.Drawing.Size(287, 39);
            this.jobTitlecomboBoxEditAssociate.TabIndex = 18;
            // 
            // associateBuildingComboBoxEditAssociate
            // 
            this.associateBuildingComboBoxEditAssociate.FormattingEnabled = true;
            this.associateBuildingComboBoxEditAssociate.Location = new System.Drawing.Point(30, 665);
            this.associateBuildingComboBoxEditAssociate.Name = "associateBuildingComboBoxEditAssociate";
            this.associateBuildingComboBoxEditAssociate.Size = new System.Drawing.Size(287, 39);
            this.associateBuildingComboBoxEditAssociate.TabIndex = 19;
            // 
            // associateBuildingLabelEditAssociate
            // 
            this.associateBuildingLabelEditAssociate.AutoEllipsis = true;
            this.associateBuildingLabelEditAssociate.AutoSize = true;
            this.associateBuildingLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.associateBuildingLabelEditAssociate.Location = new System.Drawing.Point(345, 672);
            this.associateBuildingLabelEditAssociate.Name = "associateBuildingLabelEditAssociate";
            this.associateBuildingLabelEditAssociate.Size = new System.Drawing.Size(103, 32);
            this.associateBuildingLabelEditAssociate.TabIndex = 20;
            this.associateBuildingLabelEditAssociate.Text = "Building";
            // 
            // deleteAssociateButtonEditAssociate
            // 
            this.deleteAssociateButtonEditAssociate.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteAssociateButtonEditAssociate.ForeColor = System.Drawing.Color.Red;
            this.deleteAssociateButtonEditAssociate.Location = new System.Drawing.Point(810, 174);
            this.deleteAssociateButtonEditAssociate.Name = "deleteAssociateButtonEditAssociate";
            this.deleteAssociateButtonEditAssociate.Size = new System.Drawing.Size(164, 89);
            this.deleteAssociateButtonEditAssociate.TabIndex = 21;
            this.deleteAssociateButtonEditAssociate.Text = "Delete Associate";
            this.deleteAssociateButtonEditAssociate.UseVisualStyleBackColor = true;
            this.deleteAssociateButtonEditAssociate.Click += new System.EventHandler(this.DeleteAssociateButtonEditAssociate_Click);
            // 
            // employeeEmailTextBoxEditAssociate
            // 
            this.employeeEmailTextBoxEditAssociate.Location = new System.Drawing.Point(30, 729);
            this.employeeEmailTextBoxEditAssociate.Name = "employeeEmailTextBoxEditAssociate";
            this.employeeEmailTextBoxEditAssociate.Size = new System.Drawing.Size(287, 38);
            this.employeeEmailTextBoxEditAssociate.TabIndex = 23;
            // 
            // employeeEmailLabelEditAssociate
            // 
            this.employeeEmailLabelEditAssociate.AutoEllipsis = true;
            this.employeeEmailLabelEditAssociate.AutoSize = true;
            this.employeeEmailLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeEmailLabelEditAssociate.Location = new System.Drawing.Point(345, 735);
            this.employeeEmailLabelEditAssociate.Name = "employeeEmailLabelEditAssociate";
            this.employeeEmailLabelEditAssociate.Size = new System.Drawing.Size(184, 32);
            this.employeeEmailLabelEditAssociate.TabIndex = 24;
            this.employeeEmailLabelEditAssociate.Text = "Employee Email";
            // 
            // NewAssociateButtonEditAssociate
            // 
            this.NewAssociateButtonEditAssociate.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewAssociateButtonEditAssociate.ForeColor = System.Drawing.Color.Black;
            this.NewAssociateButtonEditAssociate.Location = new System.Drawing.Point(810, 352);
            this.NewAssociateButtonEditAssociate.Name = "NewAssociateButtonEditAssociate";
            this.NewAssociateButtonEditAssociate.Size = new System.Drawing.Size(164, 89);
            this.NewAssociateButtonEditAssociate.TabIndex = 25;
            this.NewAssociateButtonEditAssociate.Text = "New Associate";
            this.NewAssociateButtonEditAssociate.UseVisualStyleBackColor = true;
            this.NewAssociateButtonEditAssociate.Click += new System.EventHandler(this.NewAssociateButtonEditAssociate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.associateComboBoxEditAssociate);
            this.groupBox1.Controls.Add(this.supervisorComboBoxEditAssociate);
            this.groupBox1.Controls.Add(this.supervisorLabelEditAssociate);
            this.groupBox1.Controls.Add(this.associateLabelEditAssociate);
            this.groupBox1.Location = new System.Drawing.Point(18, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 175);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Supervisor and Employee";
            // 
            // supervisorCheckBoxEditAssociate
            // 
            this.supervisorCheckBoxEditAssociate.AutoSize = true;
            this.supervisorCheckBoxEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.supervisorCheckBoxEditAssociate.Location = new System.Drawing.Point(370, 422);
            this.supervisorCheckBoxEditAssociate.Name = "supervisorCheckBoxEditAssociate";
            this.supervisorCheckBoxEditAssociate.Size = new System.Drawing.Size(160, 36);
            this.supervisorCheckBoxEditAssociate.TabIndex = 27;
            this.supervisorCheckBoxEditAssociate.Text = "Supervisor?";
            this.supervisorCheckBoxEditAssociate.UseVisualStyleBackColor = true;
            // 
            // reportsToComboBoxEditAssociate
            // 
            this.reportsToComboBoxEditAssociate.FormattingEnabled = true;
            this.reportsToComboBoxEditAssociate.Location = new System.Drawing.Point(651, 707);
            this.reportsToComboBoxEditAssociate.Name = "reportsToComboBoxEditAssociate";
            this.reportsToComboBoxEditAssociate.Size = new System.Drawing.Size(287, 39);
            this.reportsToComboBoxEditAssociate.TabIndex = 28;
            // 
            // reportsToLabelEditAssociate
            // 
            this.reportsToLabelEditAssociate.AutoEllipsis = true;
            this.reportsToLabelEditAssociate.AutoSize = true;
            this.reportsToLabelEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.reportsToLabelEditAssociate.Location = new System.Drawing.Point(966, 714);
            this.reportsToLabelEditAssociate.Name = "reportsToLabelEditAssociate";
            this.reportsToLabelEditAssociate.Size = new System.Drawing.Size(135, 32);
            this.reportsToLabelEditAssociate.TabIndex = 29;
            this.reportsToLabelEditAssociate.Text = "Reports to?";
            // 
            // associateLastNameLabelEditAssociateForm
            // 
            this.associateLastNameLabelEditAssociateForm.AutoSize = true;
            this.associateLastNameLabelEditAssociateForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.associateLastNameLabelEditAssociateForm.Location = new System.Drawing.Point(332, 262);
            this.associateLastNameLabelEditAssociateForm.Name = "associateLastNameLabelEditAssociateForm";
            this.associateLastNameLabelEditAssociateForm.Size = new System.Drawing.Size(127, 32);
            this.associateLastNameLabelEditAssociateForm.TabIndex = 31;
            this.associateLastNameLabelEditAssociateForm.Text = "Last Name";
            // 
            // lastNameTextBoxEditAssociateForm
            // 
            this.lastNameTextBoxEditAssociateForm.Location = new System.Drawing.Point(30, 256);
            this.lastNameTextBoxEditAssociateForm.Name = "lastNameTextBoxEditAssociateForm";
            this.lastNameTextBoxEditAssociateForm.Size = new System.Drawing.Size(287, 38);
            this.lastNameTextBoxEditAssociateForm.TabIndex = 30;
            // 
            // adminCheckBoxEditAssociate
            // 
            this.adminCheckBoxEditAssociate.AutoSize = true;
            this.adminCheckBoxEditAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.adminCheckBoxEditAssociate.Location = new System.Drawing.Point(555, 422);
            this.adminCheckBoxEditAssociate.Name = "adminCheckBoxEditAssociate";
            this.adminCheckBoxEditAssociate.Size = new System.Drawing.Size(118, 36);
            this.adminCheckBoxEditAssociate.TabIndex = 32;
            this.adminCheckBoxEditAssociate.Text = "Admin?";
            this.adminCheckBoxEditAssociate.UseVisualStyleBackColor = true;
            // 
            // EditAssociateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1132, 819);
            this.Controls.Add(this.adminCheckBoxEditAssociate);
            this.Controls.Add(this.associateLastNameLabelEditAssociateForm);
            this.Controls.Add(this.lastNameTextBoxEditAssociateForm);
            this.Controls.Add(this.reportsToLabelEditAssociate);
            this.Controls.Add(this.reportsToComboBoxEditAssociate);
            this.Controls.Add(this.supervisorCheckBoxEditAssociate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NewAssociateButtonEditAssociate);
            this.Controls.Add(this.employeeEmailLabelEditAssociate);
            this.Controls.Add(this.employeeEmailTextBoxEditAssociate);
            this.Controls.Add(this.backButtonEditAssociate);
            this.Controls.Add(this.deleteAssociateButtonEditAssociate);
            this.Controls.Add(this.associateBuildingLabelEditAssociate);
            this.Controls.Add(this.associateBuildingComboBoxEditAssociate);
            this.Controls.Add(this.jobTitlecomboBoxEditAssociate);
            this.Controls.Add(this.jobTitlelabelEditAssociate);
            this.Controls.Add(this.employeeIDLabelEditAssociate);
            this.Controls.Add(this.employeeIDtextBoxEditAssociate);
            this.Controls.Add(this.associateFirstNameLabelEditAssociate);
            this.Controls.Add(this.firstNameTextBoxEditAssociate);
            this.Controls.Add(this.dateOfTrainingCalendarEditAssociate);
            this.Controls.Add(this.employeeStartDateLabelEditAssociate);
            this.Controls.Add(this.saveButtonEditAssociate);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "EditAssociateForm";
            this.Text = "EditAssociateForm";
            this.Load += new System.EventHandler(this.EditAssociateForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button backButtonEditAssociate;
        private System.Windows.Forms.ComboBox supervisorComboBoxEditAssociate;
        private System.Windows.Forms.Label supervisorLabelEditAssociate;
        private System.Windows.Forms.Label associateLabelEditAssociate;
        private System.Windows.Forms.ComboBox associateComboBoxEditAssociate;
        private System.Windows.Forms.Button saveButtonEditAssociate;
        private System.Windows.Forms.Label employeeStartDateLabelEditAssociate;
        private System.Windows.Forms.MonthCalendar dateOfTrainingCalendarEditAssociate;
        private System.Windows.Forms.TextBox firstNameTextBoxEditAssociate;
        private System.Windows.Forms.Label associateFirstNameLabelEditAssociate;
        private System.Windows.Forms.TextBox employeeIDtextBoxEditAssociate;
        private System.Windows.Forms.Label employeeIDLabelEditAssociate;
        private System.Windows.Forms.Label jobTitlelabelEditAssociate;
        private System.Windows.Forms.ComboBox jobTitlecomboBoxEditAssociate;
        private System.Windows.Forms.ComboBox associateBuildingComboBoxEditAssociate;
        private System.Windows.Forms.Label associateBuildingLabelEditAssociate;
        private System.Windows.Forms.Button deleteAssociateButtonEditAssociate;
        private System.Windows.Forms.TextBox employeeEmailTextBoxEditAssociate;
        private System.Windows.Forms.Label employeeEmailLabelEditAssociate;
        private System.Windows.Forms.Button NewAssociateButtonEditAssociate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox supervisorCheckBoxEditAssociate;
        private System.Windows.Forms.ComboBox reportsToComboBoxEditAssociate;
        private System.Windows.Forms.Label reportsToLabelEditAssociate;
        private System.Windows.Forms.Label associateLastNameLabelEditAssociateForm;
        private System.Windows.Forms.TextBox lastNameTextBoxEditAssociateForm;
        private System.Windows.Forms.CheckBox adminCheckBoxEditAssociate;
    }
}