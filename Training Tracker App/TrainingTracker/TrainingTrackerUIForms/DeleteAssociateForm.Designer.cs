﻿namespace TrainingTrackerUIForms
{
    partial class DeleteAssociateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteAssociateForm));
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate = new System.Windows.Forms.CheckBox();
            this.deleteAssociateButtonDeleteAssociate = new System.Windows.Forms.Button();
            this.cancelButtonDeleteAssociateForm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // deleteAssociateConfirmationCheckBoxDeleteAssociate
            // 
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.AutoSize = true;
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.Location = new System.Drawing.Point(25, 79);
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.Name = "deleteAssociateConfirmationCheckBoxDeleteAssociate";
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.Size = new System.Drawing.Size(539, 36);
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.TabIndex = 2;
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.Text = "Are you sure you want to delete this associate?";
            this.deleteAssociateConfirmationCheckBoxDeleteAssociate.UseVisualStyleBackColor = true;
            // 
            // deleteAssociateButtonDeleteAssociate
            // 
            this.deleteAssociateButtonDeleteAssociate.ForeColor = System.Drawing.Color.Red;
            this.deleteAssociateButtonDeleteAssociate.Location = new System.Drawing.Point(345, 188);
            this.deleteAssociateButtonDeleteAssociate.Name = "deleteAssociateButtonDeleteAssociate";
            this.deleteAssociateButtonDeleteAssociate.Size = new System.Drawing.Size(156, 78);
            this.deleteAssociateButtonDeleteAssociate.TabIndex = 3;
            this.deleteAssociateButtonDeleteAssociate.Text = "Delete";
            this.deleteAssociateButtonDeleteAssociate.UseVisualStyleBackColor = true;
            this.deleteAssociateButtonDeleteAssociate.Click += new System.EventHandler(this.DeleteAssociateButtonDeleteAssociate_Click);
            // 
            // cancelButtonDeleteAssociateForm
            // 
            this.cancelButtonDeleteAssociateForm.ForeColor = System.Drawing.Color.Black;
            this.cancelButtonDeleteAssociateForm.Location = new System.Drawing.Point(57, 188);
            this.cancelButtonDeleteAssociateForm.Name = "cancelButtonDeleteAssociateForm";
            this.cancelButtonDeleteAssociateForm.Size = new System.Drawing.Size(156, 78);
            this.cancelButtonDeleteAssociateForm.TabIndex = 4;
            this.cancelButtonDeleteAssociateForm.Text = "Cancel";
            this.cancelButtonDeleteAssociateForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cancelButtonDeleteAssociateForm.UseVisualStyleBackColor = true;
            this.cancelButtonDeleteAssociateForm.Click += new System.EventHandler(this.CancelButtonDeleteAssociateForm_Click);
            // 
            // DeleteAssociateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(570, 304);
            this.Controls.Add(this.cancelButtonDeleteAssociateForm);
            this.Controls.Add(this.deleteAssociateButtonDeleteAssociate);
            this.Controls.Add(this.deleteAssociateConfirmationCheckBoxDeleteAssociate);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "DeleteAssociateForm";
            this.Text = "DeleteAssociateForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox deleteAssociateConfirmationCheckBoxDeleteAssociate;
        private System.Windows.Forms.Button deleteAssociateButtonDeleteAssociate;
        private System.Windows.Forms.Button cancelButtonDeleteAssociateForm;
    }
}