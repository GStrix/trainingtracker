﻿namespace TrainingTrackerUIForms
{
    partial class AddNewAssociateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNewAssociateForm));
            this.buildingLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.buildingComboBoxAddNewAssociate = new System.Windows.Forms.ComboBox();
            this.firstNameLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.firstNameTextBoxAddNewAssociate = new System.Windows.Forms.TextBox();
            this.startDateAddNewAssociate = new System.Windows.Forms.Label();
            this.emailAddressLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.emailAddressTextBoxAddNewAssociate = new System.Windows.Forms.TextBox();
            this.adminCheckBoxAddNewAssociate = new System.Windows.Forms.CheckBox();
            this.passwordLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.passwordTextBoxAddNewAssociate = new System.Windows.Forms.TextBox();
            this.addAssociateButtonAddNewAssociate = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.employeeIDLabelAddNewAssociateForm = new System.Windows.Forms.Label();
            this.jobTitleComboBoxAddNewAssociate = new System.Windows.Forms.ComboBox();
            this.employeeIDTextBoxAddNewAssociate = new System.Windows.Forms.TextBox();
            this.jobTitleLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.backButtonAddNewAssociate = new System.Windows.Forms.Button();
            this.supervisorCheckBoxAddNewAssociate = new System.Windows.Forms.CheckBox();
            this.supervisorLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.supervisorComboBoxAddNewAssociate = new System.Windows.Forms.ComboBox();
            this.lastNameTextBoxAddNewAssociate = new System.Windows.Forms.TextBox();
            this.lastNameLabelAddNewAssociate = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buildingLabelAddNewAssociate
            // 
            this.buildingLabelAddNewAssociate.AutoSize = true;
            this.buildingLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buildingLabelAddNewAssociate.Location = new System.Drawing.Point(25, 21);
            this.buildingLabelAddNewAssociate.Name = "buildingLabelAddNewAssociate";
            this.buildingLabelAddNewAssociate.Size = new System.Drawing.Size(103, 32);
            this.buildingLabelAddNewAssociate.TabIndex = 0;
            this.buildingLabelAddNewAssociate.Text = "Building";
            // 
            // buildingComboBoxAddNewAssociate
            // 
            this.buildingComboBoxAddNewAssociate.FormattingEnabled = true;
            this.buildingComboBoxAddNewAssociate.Location = new System.Drawing.Point(134, 18);
            this.buildingComboBoxAddNewAssociate.Name = "buildingComboBoxAddNewAssociate";
            this.buildingComboBoxAddNewAssociate.Size = new System.Drawing.Size(194, 39);
            this.buildingComboBoxAddNewAssociate.TabIndex = 1;
            // 
            // firstNameLabelAddNewAssociate
            // 
            this.firstNameLabelAddNewAssociate.AutoSize = true;
            this.firstNameLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.firstNameLabelAddNewAssociate.Location = new System.Drawing.Point(25, 100);
            this.firstNameLabelAddNewAssociate.Name = "firstNameLabelAddNewAssociate";
            this.firstNameLabelAddNewAssociate.Size = new System.Drawing.Size(135, 32);
            this.firstNameLabelAddNewAssociate.TabIndex = 2;
            this.firstNameLabelAddNewAssociate.Text = "First Name:";
            // 
            // firstNameTextBoxAddNewAssociate
            // 
            this.firstNameTextBoxAddNewAssociate.Location = new System.Drawing.Point(227, 100);
            this.firstNameTextBoxAddNewAssociate.Name = "firstNameTextBoxAddNewAssociate";
            this.firstNameTextBoxAddNewAssociate.Size = new System.Drawing.Size(232, 38);
            this.firstNameTextBoxAddNewAssociate.TabIndex = 3;
            // 
            // startDateAddNewAssociate
            // 
            this.startDateAddNewAssociate.AutoSize = true;
            this.startDateAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.startDateAddNewAssociate.Location = new System.Drawing.Point(25, 261);
            this.startDateAddNewAssociate.Name = "startDateAddNewAssociate";
            this.startDateAddNewAssociate.Size = new System.Drawing.Size(125, 32);
            this.startDateAddNewAssociate.TabIndex = 6;
            this.startDateAddNewAssociate.Text = "Start Date:";
            // 
            // emailAddressLabelAddNewAssociate
            // 
            this.emailAddressLabelAddNewAssociate.AutoSize = true;
            this.emailAddressLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.emailAddressLabelAddNewAssociate.Location = new System.Drawing.Point(25, 514);
            this.emailAddressLabelAddNewAssociate.Name = "emailAddressLabelAddNewAssociate";
            this.emailAddressLabelAddNewAssociate.Size = new System.Drawing.Size(179, 32);
            this.emailAddressLabelAddNewAssociate.TabIndex = 13;
            this.emailAddressLabelAddNewAssociate.Text = "E-Mail Address:";
            // 
            // emailAddressTextBoxAddNewAssociate
            // 
            this.emailAddressTextBoxAddNewAssociate.Location = new System.Drawing.Point(210, 511);
            this.emailAddressTextBoxAddNewAssociate.Name = "emailAddressTextBoxAddNewAssociate";
            this.emailAddressTextBoxAddNewAssociate.Size = new System.Drawing.Size(321, 38);
            this.emailAddressTextBoxAddNewAssociate.TabIndex = 14;
            // 
            // adminCheckBoxAddNewAssociate
            // 
            this.adminCheckBoxAddNewAssociate.AutoSize = true;
            this.adminCheckBoxAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.adminCheckBoxAddNewAssociate.Location = new System.Drawing.Point(31, 664);
            this.adminCheckBoxAddNewAssociate.Name = "adminCheckBoxAddNewAssociate";
            this.adminCheckBoxAddNewAssociate.Size = new System.Drawing.Size(118, 36);
            this.adminCheckBoxAddNewAssociate.TabIndex = 16;
            this.adminCheckBoxAddNewAssociate.Text = "Admin?";
            this.adminCheckBoxAddNewAssociate.UseVisualStyleBackColor = true;
            // 
            // passwordLabelAddNewAssociate
            // 
            this.passwordLabelAddNewAssociate.AutoSize = true;
            this.passwordLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.passwordLabelAddNewAssociate.Location = new System.Drawing.Point(25, 598);
            this.passwordLabelAddNewAssociate.Name = "passwordLabelAddNewAssociate";
            this.passwordLabelAddNewAssociate.Size = new System.Drawing.Size(117, 32);
            this.passwordLabelAddNewAssociate.TabIndex = 17;
            this.passwordLabelAddNewAssociate.Text = "Password:";
            // 
            // passwordTextBoxAddNewAssociate
            // 
            this.passwordTextBoxAddNewAssociate.Location = new System.Drawing.Point(148, 595);
            this.passwordTextBoxAddNewAssociate.Name = "passwordTextBoxAddNewAssociate";
            this.passwordTextBoxAddNewAssociate.Size = new System.Drawing.Size(311, 38);
            this.passwordTextBoxAddNewAssociate.TabIndex = 18;
            // 
            // addAssociateButtonAddNewAssociate
            // 
            this.addAssociateButtonAddNewAssociate.Location = new System.Drawing.Point(946, 708);
            this.addAssociateButtonAddNewAssociate.Name = "addAssociateButtonAddNewAssociate";
            this.addAssociateButtonAddNewAssociate.Size = new System.Drawing.Size(205, 76);
            this.addAssociateButtonAddNewAssociate.TabIndex = 19;
            this.addAssociateButtonAddNewAssociate.Text = "Add Associate";
            this.addAssociateButtonAddNewAssociate.UseVisualStyleBackColor = true;
            this.addAssociateButtonAddNewAssociate.Click += new System.EventHandler(this.AddAssociateButtonAddNewAssociate_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(166, 235);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 20;
            // 
            // employeeIDLabelAddNewAssociateForm
            // 
            this.employeeIDLabelAddNewAssociateForm.AutoSize = true;
            this.employeeIDLabelAddNewAssociateForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeIDLabelAddNewAssociateForm.Location = new System.Drawing.Point(506, 143);
            this.employeeIDLabelAddNewAssociateForm.Name = "employeeIDLabelAddNewAssociateForm";
            this.employeeIDLabelAddNewAssociateForm.Size = new System.Drawing.Size(155, 32);
            this.employeeIDLabelAddNewAssociateForm.TabIndex = 21;
            this.employeeIDLabelAddNewAssociateForm.Text = "Employee ID:";
            // 
            // jobTitleComboBoxAddNewAssociate
            // 
            this.jobTitleComboBoxAddNewAssociate.FormattingEnabled = true;
            this.jobTitleComboBoxAddNewAssociate.Location = new System.Drawing.Point(667, 235);
            this.jobTitleComboBoxAddNewAssociate.Name = "jobTitleComboBoxAddNewAssociate";
            this.jobTitleComboBoxAddNewAssociate.Size = new System.Drawing.Size(337, 39);
            this.jobTitleComboBoxAddNewAssociate.TabIndex = 22;
            this.jobTitleComboBoxAddNewAssociate.SelectedIndexChanged += new System.EventHandler(this.JobTitleComboBoxAddNewAssociate_SelectedIndexChanged);
            // 
            // employeeIDTextBoxAddNewAssociate
            // 
            this.employeeIDTextBoxAddNewAssociate.Location = new System.Drawing.Point(667, 137);
            this.employeeIDTextBoxAddNewAssociate.Name = "employeeIDTextBoxAddNewAssociate";
            this.employeeIDTextBoxAddNewAssociate.Size = new System.Drawing.Size(232, 38);
            this.employeeIDTextBoxAddNewAssociate.TabIndex = 23;
            // 
            // jobTitleLabelAddNewAssociate
            // 
            this.jobTitleLabelAddNewAssociate.AutoSize = true;
            this.jobTitleLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.jobTitleLabelAddNewAssociate.Location = new System.Drawing.Point(538, 238);
            this.jobTitleLabelAddNewAssociate.Name = "jobTitleLabelAddNewAssociate";
            this.jobTitleLabelAddNewAssociate.Size = new System.Drawing.Size(110, 32);
            this.jobTitleLabelAddNewAssociate.TabIndex = 24;
            this.jobTitleLabelAddNewAssociate.Text = "Job Title:";
            // 
            // backButtonAddNewAssociate
            // 
            this.backButtonAddNewAssociate.Location = new System.Drawing.Point(17, 738);
            this.backButtonAddNewAssociate.Name = "backButtonAddNewAssociate";
            this.backButtonAddNewAssociate.Size = new System.Drawing.Size(111, 46);
            this.backButtonAddNewAssociate.TabIndex = 25;
            this.backButtonAddNewAssociate.Text = "Back";
            this.backButtonAddNewAssociate.UseVisualStyleBackColor = true;
            this.backButtonAddNewAssociate.Click += new System.EventHandler(this.BackButtonAddNewAssociate_Click);
            // 
            // supervisorCheckBoxAddNewAssociate
            // 
            this.supervisorCheckBoxAddNewAssociate.AutoSize = true;
            this.supervisorCheckBoxAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.supervisorCheckBoxAddNewAssociate.Location = new System.Drawing.Point(227, 664);
            this.supervisorCheckBoxAddNewAssociate.Name = "supervisorCheckBoxAddNewAssociate";
            this.supervisorCheckBoxAddNewAssociate.Size = new System.Drawing.Size(160, 36);
            this.supervisorCheckBoxAddNewAssociate.TabIndex = 26;
            this.supervisorCheckBoxAddNewAssociate.Text = "Supervisor?";
            this.supervisorCheckBoxAddNewAssociate.UseVisualStyleBackColor = true;
            // 
            // supervisorLabelAddNewAssociate
            // 
            this.supervisorLabelAddNewAssociate.AutoSize = true;
            this.supervisorLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.supervisorLabelAddNewAssociate.Location = new System.Drawing.Point(538, 363);
            this.supervisorLabelAddNewAssociate.Name = "supervisorLabelAddNewAssociate";
            this.supervisorLabelAddNewAssociate.Size = new System.Drawing.Size(132, 32);
            this.supervisorLabelAddNewAssociate.TabIndex = 27;
            this.supervisorLabelAddNewAssociate.Text = "Supervisor:";
            // 
            // supervisorComboBoxAddNewAssociate
            // 
            this.supervisorComboBoxAddNewAssociate.FormattingEnabled = true;
            this.supervisorComboBoxAddNewAssociate.Location = new System.Drawing.Point(667, 356);
            this.supervisorComboBoxAddNewAssociate.Name = "supervisorComboBoxAddNewAssociate";
            this.supervisorComboBoxAddNewAssociate.Size = new System.Drawing.Size(337, 39);
            this.supervisorComboBoxAddNewAssociate.TabIndex = 28;
            // 
            // lastNameTextBoxAddNewAssociate
            // 
            this.lastNameTextBoxAddNewAssociate.Location = new System.Drawing.Point(227, 169);
            this.lastNameTextBoxAddNewAssociate.Name = "lastNameTextBoxAddNewAssociate";
            this.lastNameTextBoxAddNewAssociate.Size = new System.Drawing.Size(232, 38);
            this.lastNameTextBoxAddNewAssociate.TabIndex = 30;
            // 
            // lastNameLabelAddNewAssociate
            // 
            this.lastNameLabelAddNewAssociate.AutoSize = true;
            this.lastNameLabelAddNewAssociate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lastNameLabelAddNewAssociate.Location = new System.Drawing.Point(25, 169);
            this.lastNameLabelAddNewAssociate.Name = "lastNameLabelAddNewAssociate";
            this.lastNameLabelAddNewAssociate.Size = new System.Drawing.Size(132, 32);
            this.lastNameLabelAddNewAssociate.TabIndex = 29;
            this.lastNameLabelAddNewAssociate.Text = "Last Name:";
            // 
            // AddNewAssociateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1198, 796);
            this.Controls.Add(this.lastNameTextBoxAddNewAssociate);
            this.Controls.Add(this.lastNameLabelAddNewAssociate);
            this.Controls.Add(this.supervisorComboBoxAddNewAssociate);
            this.Controls.Add(this.supervisorLabelAddNewAssociate);
            this.Controls.Add(this.supervisorCheckBoxAddNewAssociate);
            this.Controls.Add(this.backButtonAddNewAssociate);
            this.Controls.Add(this.jobTitleLabelAddNewAssociate);
            this.Controls.Add(this.employeeIDTextBoxAddNewAssociate);
            this.Controls.Add(this.jobTitleComboBoxAddNewAssociate);
            this.Controls.Add(this.employeeIDLabelAddNewAssociateForm);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.addAssociateButtonAddNewAssociate);
            this.Controls.Add(this.passwordTextBoxAddNewAssociate);
            this.Controls.Add(this.passwordLabelAddNewAssociate);
            this.Controls.Add(this.adminCheckBoxAddNewAssociate);
            this.Controls.Add(this.emailAddressTextBoxAddNewAssociate);
            this.Controls.Add(this.emailAddressLabelAddNewAssociate);
            this.Controls.Add(this.startDateAddNewAssociate);
            this.Controls.Add(this.firstNameTextBoxAddNewAssociate);
            this.Controls.Add(this.firstNameLabelAddNewAssociate);
            this.Controls.Add(this.buildingComboBoxAddNewAssociate);
            this.Controls.Add(this.buildingLabelAddNewAssociate);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "AddNewAssociateForm";
            this.Text = "AddNewAssociateForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label buildingLabelAddNewAssociate;
        private System.Windows.Forms.ComboBox buildingComboBoxAddNewAssociate;
        private System.Windows.Forms.Label firstNameLabelAddNewAssociate;
        private System.Windows.Forms.TextBox firstNameTextBoxAddNewAssociate;
        private System.Windows.Forms.Label startDateAddNewAssociate;
        private System.Windows.Forms.Label emailAddressLabelAddNewAssociate;
        private System.Windows.Forms.TextBox emailAddressTextBoxAddNewAssociate;
        private System.Windows.Forms.CheckBox adminCheckBoxAddNewAssociate;
        private System.Windows.Forms.Label passwordLabelAddNewAssociate;
        private System.Windows.Forms.TextBox passwordTextBoxAddNewAssociate;
        private System.Windows.Forms.Button addAssociateButtonAddNewAssociate;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label employeeIDLabelAddNewAssociateForm;
        private System.Windows.Forms.ComboBox jobTitleComboBoxAddNewAssociate;
        private System.Windows.Forms.TextBox employeeIDTextBoxAddNewAssociate;
        private System.Windows.Forms.Label jobTitleLabelAddNewAssociate;
        private System.Windows.Forms.Button backButtonAddNewAssociate;
        private System.Windows.Forms.CheckBox supervisorCheckBoxAddNewAssociate;
        private System.Windows.Forms.Label supervisorLabelAddNewAssociate;
        private System.Windows.Forms.ComboBox supervisorComboBoxAddNewAssociate;
        private System.Windows.Forms.TextBox lastNameTextBoxAddNewAssociate;
        private System.Windows.Forms.Label lastNameLabelAddNewAssociate;
    }
}