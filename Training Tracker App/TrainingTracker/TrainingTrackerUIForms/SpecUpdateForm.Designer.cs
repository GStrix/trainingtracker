﻿namespace TrainingTrackerUIForms
{
    partial class SpecUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpecUpdateForm));
            this.newSpecNameLabelSpecUpdate = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.deleteSpecButtonSpecUpdate = new System.Windows.Forms.Button();
            this.backButtonSpecUpdate = new System.Windows.Forms.Button();
            this.createNewSpecGroupBoxSpecUpdate = new System.Windows.Forms.GroupBox();
            this.deleteSpecGroupBoxSpecUpdate = new System.Windows.Forms.GroupBox();
            this.createNewSpecGroupBoxSpecUpdate.SuspendLayout();
            this.deleteSpecGroupBoxSpecUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // newSpecNameLabelSpecUpdate
            // 
            this.newSpecNameLabelSpecUpdate.AutoSize = true;
            this.newSpecNameLabelSpecUpdate.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newSpecNameLabelSpecUpdate.ForeColor = System.Drawing.Color.White;
            this.newSpecNameLabelSpecUpdate.Location = new System.Drawing.Point(128, 92);
            this.newSpecNameLabelSpecUpdate.Name = "newSpecNameLabelSpecUpdate";
            this.newSpecNameLabelSpecUpdate.Size = new System.Drawing.Size(167, 30);
            this.newSpecNameLabelSpecUpdate.TabIndex = 1;
            this.newSpecNameLabelSpecUpdate.Text = "Drag Spec Here";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(59, 43);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(225, 36);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // deleteSpecButtonSpecUpdate
            // 
            this.deleteSpecButtonSpecUpdate.Location = new System.Drawing.Point(117, 88);
            this.deleteSpecButtonSpecUpdate.Name = "deleteSpecButtonSpecUpdate";
            this.deleteSpecButtonSpecUpdate.Size = new System.Drawing.Size(110, 48);
            this.deleteSpecButtonSpecUpdate.TabIndex = 4;
            this.deleteSpecButtonSpecUpdate.Text = "Delete Spec";
            this.deleteSpecButtonSpecUpdate.UseVisualStyleBackColor = true;
            this.deleteSpecButtonSpecUpdate.Click += new System.EventHandler(this.DeleteSpecButtonSpecUpdate_Click);
            // 
            // backButtonSpecUpdate
            // 
            this.backButtonSpecUpdate.Location = new System.Drawing.Point(12, 557);
            this.backButtonSpecUpdate.Name = "backButtonSpecUpdate";
            this.backButtonSpecUpdate.Size = new System.Drawing.Size(98, 35);
            this.backButtonSpecUpdate.TabIndex = 5;
            this.backButtonSpecUpdate.Text = "Back";
            this.backButtonSpecUpdate.UseVisualStyleBackColor = true;
            this.backButtonSpecUpdate.Click += new System.EventHandler(this.BackButtonSpecUpdate_Click);
            // 
            // createNewSpecGroupBoxSpecUpdate
            // 
            this.createNewSpecGroupBoxSpecUpdate.Controls.Add(this.newSpecNameLabelSpecUpdate);
            this.createNewSpecGroupBoxSpecUpdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewSpecGroupBoxSpecUpdate.Location = new System.Drawing.Point(96, 99);
            this.createNewSpecGroupBoxSpecUpdate.Name = "createNewSpecGroupBoxSpecUpdate";
            this.createNewSpecGroupBoxSpecUpdate.Size = new System.Drawing.Size(431, 209);
            this.createNewSpecGroupBoxSpecUpdate.TabIndex = 6;
            this.createNewSpecGroupBoxSpecUpdate.TabStop = false;
            this.createNewSpecGroupBoxSpecUpdate.Text = "Create a New Spec";
            // 
            // deleteSpecGroupBoxSpecUpdate
            // 
            this.deleteSpecGroupBoxSpecUpdate.Controls.Add(this.deleteSpecButtonSpecUpdate);
            this.deleteSpecGroupBoxSpecUpdate.Controls.Add(this.comboBox1);
            this.deleteSpecGroupBoxSpecUpdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSpecGroupBoxSpecUpdate.Location = new System.Drawing.Point(122, 314);
            this.deleteSpecGroupBoxSpecUpdate.Name = "deleteSpecGroupBoxSpecUpdate";
            this.deleteSpecGroupBoxSpecUpdate.Size = new System.Drawing.Size(356, 148);
            this.deleteSpecGroupBoxSpecUpdate.TabIndex = 7;
            this.deleteSpecGroupBoxSpecUpdate.TabStop = false;
            this.deleteSpecGroupBoxSpecUpdate.Text = "Delete Spec";
            // 
            // SpecUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(636, 604);
            this.Controls.Add(this.deleteSpecGroupBoxSpecUpdate);
            this.Controls.Add(this.createNewSpecGroupBoxSpecUpdate);
            this.Controls.Add(this.backButtonSpecUpdate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpecUpdateForm";
            this.Text = "SpecUpdateForm";
            this.createNewSpecGroupBoxSpecUpdate.ResumeLayout(false);
            this.createNewSpecGroupBoxSpecUpdate.PerformLayout();
            this.deleteSpecGroupBoxSpecUpdate.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label newSpecNameLabelSpecUpdate;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button deleteSpecButtonSpecUpdate;
        private System.Windows.Forms.Button backButtonSpecUpdate;
        private System.Windows.Forms.GroupBox createNewSpecGroupBoxSpecUpdate;
        private System.Windows.Forms.GroupBox deleteSpecGroupBoxSpecUpdate;
    }
}