﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TrainingTrackerLibrary;

namespace TrainingTrackerUIForms
{
    public partial class HubForm : Form
    {
        public HubForm()
        {

            //TODO: get columns set up so they can be aded variably by procedure and then add them variably by procedure to DGV
            InitializeComponent();
            dataGridViewMainViewer.AutoGenerateColumns = false;

            List<AssociateModel> Associates = LoadAssociates();
            List<String> procedureNames = LoadProcedureTitles();

            DataTable dt = new DataTable();
            dt.Columns.Add("Select", typeof(bool));
            dt.Columns.Add("Last Name", typeof(string));
            dt.Columns.Add("First Name", typeof(string));
            dt.Columns.Add("EmployeeID", typeof(int));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("StartDate", typeof(string));
            dt.Columns.Add("Supervisor", typeof(string));
            dt.Columns.Add("Supervisor Status", typeof(bool));
            dt.Columns.Add("Admin Access", typeof(bool));
            dt.Columns.Add("Job Title", typeof(string));
            foreach(String procedure in procedureNames)
            {
                dt.Columns.Add(procedure, typeof(bool));
            }
            /*dt.Columns.Add("Sorting", typeof(bool));
            dt.Columns.Add("Picking", typeof(bool));
            dt.Columns.Add("Filling", typeof(bool));
            dt.Columns.Add("Shipping", typeof(bool));*/
            dt = FillEmployeeDataTable(Associates, dt);

            List<DataGridViewColumn> dgColumns = new List<DataGridViewColumn>();

            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.HeaderText = "Select";
            select.DataPropertyName = "Select";
            select.Name = "Select";
            dgColumns.Add(select);

            /*DataGridViewCheckBoxColumn sorting = new DataGridViewCheckBoxColumn();
            sorting.HeaderText = "Sorting";
            sorting.DataPropertyName = "Sorting";

            DataGridViewCheckBoxColumn picking = new DataGridViewCheckBoxColumn();
            picking.HeaderText = "Picking";
            picking.DataPropertyName = "Picking";

            DataGridViewCheckBoxColumn filling = new DataGridViewCheckBoxColumn();
            filling.HeaderText = "Filling";
            filling.DataPropertyName = "Filling";

            DataGridViewCheckBoxColumn shipping = new DataGridViewCheckBoxColumn();
            shipping.HeaderText = "Shipping";
            shipping.DataPropertyName = "Shipping";*/

            DataGridViewTextBoxColumn lastName = new DataGridViewTextBoxColumn();
            lastName.HeaderText = "Last Name";
            lastName.DataPropertyName = "Last Name";
            lastName.Name = "Last Name";
            dgColumns.Add(lastName);

            DataGridViewTextBoxColumn firstName = new DataGridViewTextBoxColumn();
            firstName.HeaderText = "First Name";
            firstName.DataPropertyName = "First Name";
            firstName.Name = "First Name";
            dgColumns.Add(firstName);

            foreach (String procedureTitle in procedureNames)
            {
                DataGridViewCheckBoxColumn procedureToAdd = new DataGridViewCheckBoxColumn();
                procedureToAdd.HeaderText = procedureTitle;
                procedureToAdd.DataPropertyName = procedureTitle;
                procedureToAdd.Name = procedureTitle;
                procedureToAdd.ReadOnly = true;
                dgColumns.Add(procedureToAdd);
            }

            DataGridViewTextBoxColumn startDate = new DataGridViewTextBoxColumn();
            startDate.HeaderText = "StartDate";
            startDate.DataPropertyName = "StartDate";
            startDate.Name = "StartDate";
            dgColumns.Add(startDate);

            DataGridViewTextBoxColumn email = new DataGridViewTextBoxColumn();
            email.HeaderText = "Email";
            email.DataPropertyName = "Email";
            dgColumns.Add(email);

            DataGridViewComboBoxColumn jobTitle = new DataGridViewComboBoxColumn();
            var jobTitleDropDownList = LoadJobTitles();
            jobTitleDropDownList.Add("****");
            jobTitle.DataSource = jobTitleDropDownList;
            jobTitle.HeaderText = "Job Title";
            jobTitle.DataPropertyName = "Job Title";
            jobTitle.Name = "Job Title";
            dgColumns.Add(jobTitle);

            DataGridViewComboBoxColumn supervisor = new DataGridViewComboBoxColumn();
            var supervisorDropDownList = LoadSupervisors();
            supervisorDropDownList.Add("****");
            supervisor.DataSource = supervisorDropDownList;
            supervisor.HeaderText = "Supervisor";
            supervisor.DataPropertyName = "Supervisor";
            supervisor.Name = "Supervisor";
            dgColumns.Add(supervisor);

            DataGridViewTextBoxColumn employeeID = new DataGridViewTextBoxColumn();
            employeeID.HeaderText = "EmployeeID";
            employeeID.DataPropertyName = "EmployeeID";
            employeeID.Name = "EmployeeID";
            dgColumns.Add(employeeID);

            DataGridViewCheckBoxColumn supervisorStatus = new DataGridViewCheckBoxColumn();
            supervisorStatus.HeaderText = "Supervisor Status";
            supervisorStatus.DataPropertyName = "Supervisor Status";
            supervisorStatus.Name = "Supervisor Status";
            dgColumns.Add(supervisorStatus);

            DataGridViewCheckBoxColumn adminAccess = new DataGridViewCheckBoxColumn();
            adminAccess.HeaderText = "Admin Access";
            adminAccess.DataPropertyName = "Admin Access";
            adminAccess.Name = "Admin Access";
            dgColumns.Add(adminAccess);


            dataGridViewMainViewer.DataSource = dt;
            /*dataGridViewMainViewer.Columns.AddRange(select);
            dataGridViewMainViewer.Columns.AddRange(lastName);
            dataGridViewMainViewer.Columns.AddRange(firstName);
            dataGridViewMainViewer.Columns.AddRange(employeeID);
            dataGridViewMainViewer.Columns.AddRange(jobTitle);*/
            foreach(DataGridViewColumn column in dgColumns)
            {
                dataGridViewMainViewer.Columns.AddRange(column);
                //dataGridViewMainViewer.Columns[column.Name].ReadOnly = true; //Currently makes everything readonly
            }
            /*dataGridViewMainViewer.Columns.AddRange(sorting);
            dataGridViewMainViewer.Columns.AddRange(picking);
            dataGridViewMainViewer.Columns.AddRange(filling);
            dataGridViewMainViewer.Columns.AddRange(shipping);
            dataGridViewMainViewer.Columns.AddRange(email);
            dataGridViewMainViewer.Columns.AddRange(startDate);
            dataGridViewMainViewer.Columns.AddRange(supervisor);
            dataGridViewMainViewer.Columns.AddRange(supervisorStatus);
            dataGridViewMainViewer.Columns.AddRange(adminAccess);*/


            dataGridViewMainViewer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;


        }

        private void TrainingButtonHubForm_Click(object sender, EventArgs e)
        {
            var trainingForm = new TrainingForm();
            trainingForm.Show();
            this.Close();
        }

        private void EmployeesButtonHub_Click(object sender, EventArgs e)
        {
            var associateForm = new EditAssociateForm();
            associateForm.Show();
            this.Close();
        }

        private void SpecsButtonHub_Click(object sender, EventArgs e)
        {
            var specsForm = new SpecUpdateForm();
            specsForm.Show();
            this.Close();
        }

        private void ProceduresButtonHub_Click(object sender, EventArgs e)
        {
            var proceduresForm = new EditProcedureForm();
            proceduresForm.Show();
            this.Close();
        }

        private void ErrorsButtonHub_Click(object sender, EventArgs e)
        {
            var errorsForm = new ErrorsForm();
            errorsForm.Show();
            this.Close();
        }

        private DataTable FillEmployeeDataTable(List<AssociateModel> associates, DataTable table)
        {
            var editProcedureFormRef = new EditProcedureForm();

            List<String> procedureTitles = LoadProcedureTitles();
            DataRow dataRow = null;
            foreach (AssociateModel assc in associates)
            {
                dataRow = table.NewRow();
                dataRow["Select"] = false;
                dataRow["Last Name"] = assc.LastName;
                dataRow["First Name"] = assc.FirstName;
                dataRow["EmployeeID"] = assc.employeeId;
                dataRow["Job Title"] = assc.jobTitle;
                foreach (String procedureTitle in procedureTitles)
                {
                    dataRow[procedureTitle] = CheckAssociateProcedureAgainstSpecsNeededForProcedure(editProcedureFormRef.LoadSpecsAssociatedToProcedure(procedureTitle), assc.FirstName + " " + assc.LastName);
                }
                /*dataRow["Sorting"] = CheckAssociateProcedureAgainstSpecsNeededForProcedure(editProcedureFormRef.LoadSpecsAssociatedToProcedure("Sorting"), assc.FirstName + " " + assc.LastName);
                dataRow["Picking"] = CheckAssociateProcedureAgainstSpecsNeededForProcedure(editProcedureFormRef.LoadSpecsAssociatedToProcedure("Picking"), assc.FirstName + " " + assc.LastName);
                dataRow["Filling"] = CheckAssociateProcedureAgainstSpecsNeededForProcedure(editProcedureFormRef.LoadSpecsAssociatedToProcedure("Filling"), assc.FirstName + " " + assc.LastName);
                dataRow["Shipping"] = CheckAssociateProcedureAgainstSpecsNeededForProcedure(editProcedureFormRef.LoadSpecsAssociatedToProcedure("Shipping"), assc.FirstName + " " + assc.LastName);*/
                dataRow["Email"] = assc.EmailAddress;
                dataRow["StartDate"] = assc.StartDate;
                dataRow["Supervisor"] = assc.supervisor;
                dataRow["Supervisor Status"] = assc.SupervisorStatus;
                dataRow["Admin Access"] = assc.AdminAccess;
                
                table.Rows.Add(dataRow);
            }
            return table;
        }

        public List<AssociateModel> LoadAssociates()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path + "\\", "Training Tracker App\\TrainingTracker\\Associates");
            string[] folderArray = Directory.GetDirectories(extendedAssociatePath);
            var associateList = new List<AssociateModel>();
            foreach (var directory in folderArray)
            {
                var associate = new AssociateModel();
                string[] fileArray = Directory.GetFiles(Path.Combine(extendedAssociatePath, directory));
                foreach (var file in fileArray) if (file.EndsWith("Data.txt"))
                {
                        string[] lines = File.ReadAllLines(file);
                        associate.FirstName = lines[0];
                        associate.LastName = lines[1];
                        associate.employeeId = lines[2];
                        associate.EmailAddress = lines[3];
                        var dateTime = DateTime.Parse(lines[4]);
                        associate.StartDate = dateTime.Date.ToShortDateString();
                        associate.supervisor = lines[5];
                        if (lines[6] == "Y")
                            associate.SupervisorStatus = true;
                        else
                            associate.SupervisorStatus = false;
                        if (lines[7] == "Y")
                            associate.AdminAccess = true;
                        else
                            associate.AdminAccess = false;
                        associate.Password = lines[8];
                        associate.jobTitle = lines[9];
                        associateList.Add(associate);
                }


                string[] specFilesArray = Directory.GetFiles(extendedAssociatePath + "\\" + associate.FirstName + " " + associate.LastName  + "\\Specs" );
                foreach (var file in specFilesArray)
                {
                    var specModel = new SpecModel();
                    string[] lines = File.ReadAllLines(file);
                    if(lines.Length > 0)
                    {
                        specModel.SpecName = Path.GetFileNameWithoutExtension(file);
                        specModel.SpecCompletionDate = lines[0];
                        specModel.RevisionVersion = lines[1];
                        associate.Specs.Add(specModel);
                    }
                    
                }
            }
            
            return associateList;
        }

        public List<String> AssociateToString(List<AssociateModel> asscs)
        {
            List<String> associateNames = new List<String>();
            foreach (var assc in asscs)
            {
                associateNames.Add(assc.FirstName + " " + assc.LastName);
            }
            return associateNames;
        }

        public List<AssociateModel> StringToAssociate(List<String> asscNames)
        {
            List<AssociateModel> associateModels = new List<AssociateModel>();
            List<AssociateModel> associatesToCompare = new List<AssociateModel>();
            associatesToCompare = LoadAssociates();
            foreach (string name in asscNames)
            {
                foreach(var asscComparing in associatesToCompare)
                {
                    if(name.Contains(asscComparing.FirstName) && name.Contains(asscComparing.LastName))
                    {
                        associateModels.Add(asscComparing);
                        associateModels[0].Specs = asscComparing.Specs;
                        break;
                    }
                }
            }
            return associateModels;
        }

        public List<String> LoadJobTitles()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedTrainingTrackerPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\");
            string[] lines = File.ReadAllLines(extendedTrainingTrackerPath + "JobTitles.txt");
            return lines.ToList<String>();
        }

        public List<String> LoadProcedureTitles()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedTrainingTrackerPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\");
            string[] lines = File.ReadAllLines(extendedTrainingTrackerPath + "ProcedureTitles.txt");
            return lines.ToList<String>();
        }

        public List<String> LoadErrorTitles()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedTrainingTrackerPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\");
            string[] lines = File.ReadAllLines(extendedTrainingTrackerPath + "ErrorTitles.txt");
            return lines.ToList<String>();
        }


        public List<String> LoadSupervisors()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            string[] folderArray = Directory.GetDirectories(extendedAssociatePath);
            var supervisorList = new List<String>();
            foreach (var directory in folderArray)
            {
                string[] fileArray = Directory.GetFiles(Path.Combine(extendedAssociatePath + "\\", directory));
                foreach (var file in fileArray) if (file.EndsWith("Data.txt"))
                    {
                        string[] lines = File.ReadAllLines(Path.Combine(extendedAssociatePath + "\\", file));
                        if (lines[6] == "Y")
                        {
                            supervisorList.Add(lines[0] + " " + lines[1]);
                        }
                    }
                
            }
            return supervisorList;
        }

        public bool CheckAssociateProcedureAgainstSpecsNeededForProcedure(List<string> specsAssociated, string asscName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            int specCount = 0;
            if (asscName != null)
            {
               
                List<string> asscNames = new List<string>();
                asscNames.Add(asscName);
                List<AssociateModel> asscModel = StringToAssociate(asscNames);
                string filePath = extendedAssociatePath + "\\" + asscName + "\\Specs";
                if (Directory.Exists(filePath))
                {
                    foreach (SpecModel spec in asscModel.First().Specs) if (specsAssociated.Contains(spec.SpecName))
                    {
                            if (spec.RevisionVersion != "N/A" || spec.SpecCompletionDate != "N/A")
                            {
                                specCount++;
                            }
                            
                    }

                }

            }
            if (specCount == specsAssociated.Count() && specCount > 0)
            {
                return true;
            }
            else return false;

        }

        private void DataGridViewMainViewer_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            if (dataGridViewMainViewer.IsCurrentCellDirty)
            {
                dataGridViewMainViewer.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (dataGridViewMainViewer.Columns[e.ColumnIndex].Name != "Last Name" && dataGridViewMainViewer.Columns[e.ColumnIndex].Name != "First Name")
                {
                    DataTable dt = (DataTable)dataGridViewMainViewer.DataSource;
                    string newFolderName = dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["First Name"].Index].ToString() + " " + dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Last Name"].Index].ToString();
                    string newFilePath = Path.Combine(extendedAssociatePath, newFolderName + "\\Data.txt");
                    //File.Delete(newFilePath); BRING THIS BACK AFTER TESTING!!!!!
                    if (!File.Exists(newFilePath))
                    {
                        using (StreamWriter sw = File.CreateText(newFilePath)) 
                        {
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["First Name"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Last Name"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["EmployeeID"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Email"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["StartDate"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Supervisor"].Index].ToString());
                            if ((bool)dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Supervisor Status"].Index])
                            {
                                sw.WriteLine("Y");
                            }
                            else
                                sw.WriteLine("N");
                            if ((bool)dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Admin Access"].Index])
                            {
                                sw.WriteLine("Y");
                            }
                            else
                                sw.WriteLine("N");
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Password"].Index].ToString());
                            sw.WriteLine(dt.Rows[e.RowIndex][dataGridViewMainViewer.Columns["Job Title"].Index].ToString());
                        }

                    }
                    dataGridViewMainViewer.Invalidate();

                }
            }

        }

        private void DataGridViewMainViewer_CellContentClick(object sender, DataGridViewCellEventArgs e)//Change values when a cell is clicked FIGURE THIS OUT
        {
            
        }

        private void DataGridMainViewer_CurrentCellDirtyStateChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewMainViewer.IsCurrentCellDirty)
            {
                dataGridViewMainViewer.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

    }
}
