﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TrainingTrackerLibrary;

namespace TrainingTrackerUIForms
{
    public partial class EditProcedureForm : Form
    {
        public EditProcedureForm()
        {
            InitializeComponent();
            var specUpdateFormRef = new SpecUpdateForm();
            currentSpecsDataGridViewEditProcedure.AutoGenerateColumns = false;


            DataTable dt = new DataTable();
            dt.Columns.Add("Select", typeof(bool));
            dt.Columns.Add("Spec Name", typeof(string));

            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.HeaderText = "Select";
            select.DataPropertyName = "Select";

            DataGridViewTextBoxColumn firstName = new DataGridViewTextBoxColumn();
            firstName.HeaderText = "Spec Name";
            firstName.DataPropertyName = "Spec Name";

            currentSpecsDataGridViewEditProcedure.DataSource = dt;
            currentSpecsDataGridViewEditProcedure.Columns.AddRange(select);
            currentSpecsDataGridViewEditProcedure.Columns.AddRange(firstName);

            currentSpecsDataGridViewEditProcedure.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            FillSpecDataTable(specUpdateFormRef.LoadSpecs(), dt);
            procedureToChangeComboBoxEditProcedureForm.DataSource = LoadProcedures();
            newProcedureComboBoxEditProcedureForm.DataSource = specUpdateFormRef.LoadSpecs();
        }

        private void BackButtonEditProcedure_Click(object sender, EventArgs e)
        {
            var hubForm = new HubForm();
            hubForm.Show();
            this.Close();
        }

        private void AddSpecButtonEditProcedure_Click(object sender, EventArgs e)
        {

        }

        public List<String> LoadProcedures()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures");
            string[] fileArray = Directory.GetDirectories(extendedProcedurePath);
            List<String> fileNames = new List<String>();
            fileNames.Add("****");
            foreach (var file in fileArray)
            {
                fileNames.Add(Path.GetFileName(file));
            }

            return fileNames.ToList<String>();
        }

        public List<string> LoadSpecsAssociatedToProcedure(String procedureName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures");
            var path2 = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedTrainingFormsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\");
            string[] fileArray = Directory.GetDirectories(extendedProcedurePath);
            List<string> specsAssociatedToProcedure = new List<string>();
            foreach (string file  in fileArray)
            {
                if (Path.GetFileNameWithoutExtension(file) == procedureName)
                {
                    string[] procedureFileArray = Directory.GetFiles(Path.Combine(extendedTrainingFormsPath, file));
                    foreach (var pFile in procedureFileArray) if (pFile.EndsWith("Associated.txt"))
                    {
                            string[] lines = File.ReadAllLines(pFile);
                            foreach (string line in lines)
                            {
                                specsAssociatedToProcedure.Add(line);
                            }
                    }
                            //string specAssociatedPath = Path.GetFileName(@"C:\Users\gavin\Desktop\Training Tracker App\TrainingTracker\TrainingForms\Procedures\" + procedureName + "\\SpecsAssociated.txt");
                    
                    
                }
            }
            return specsAssociatedToProcedure;
        }

        private void NewProcedureButtonEditProcedure_Click(object sender, EventArgs e)
        {
            var addProcedureForm = new AddProcedureForm();
            addProcedureForm.Show();
            this.Close();
        }


        public DataTable FillSpecDataTable(List<string> specList, DataTable dt)
        {
            foreach (string specName in specList)
            {
                DataRow dataRow = null;
                dataRow = dt.NewRow();
                dataRow["Select"] = false;
                dataRow["Spec Name"] = specName;
                dt.Rows.Add(dataRow);
            }
            return dt;

        }

        public DataTable UpdateAssociatedSpec(string procedure, DataTable dt) //Updates the datatable to show if a spec is associated with a procedure or not.
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures");
            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("Select", typeof(bool));
                dt.Columns.Add("Spec Name", typeof(string));
            }
            string[] fileArray = Directory.GetDirectories(extendedProcedurePath);
            foreach (string file in fileArray)
            {
                if (file == Path.Combine(extendedProcedurePath + "\\" + procedure))
                {
                    string[] procedureFileArray = Directory.GetFiles(Path.Combine(extendedProcedurePath + "\\", file));
                    foreach (var pFile in procedureFileArray) if (pFile.EndsWith("SpecsAssociated.txt"))
                        {
                            string[] lines = File.ReadAllLines(pFile);
                            foreach (string line in lines)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    if (row["Spec Name"].ToString() == line)
                                    {
                                        row["Select"] = true;
                                    }
                                }
                            }

                        }

                }
            }
            return dt;

        }

        private void RemoveSpecButtonEditProcedure_Click(object sender, EventArgs e)
        {

        }

        private void ProcedureToChangeComboBoxEditProcedureForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)currentSpecsDataGridViewEditProcedure.DataSource;
            foreach (DataRow dataRow in dt.Rows)
            {
                dataRow["Select"] = false;
            }
            dt = UpdateAssociatedSpec(procedureToChangeComboBoxEditProcedureForm.Text, dt);
            currentSpecsDataGridViewEditProcedure.DataSource = dt;
        }

        private void CurrentSpecsDataGridViewEditProcedure_CurrentCellDirtyStateChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (currentSpecsDataGridViewEditProcedure.IsCurrentCellDirty)
            {
                currentSpecsDataGridViewEditProcedure.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void CurrentSpecsDataGridViewEditProcedure_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
           
        }

        private void CurrentSpecsDataGridViewEditProcedure_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures\\");
            if (currentSpecsDataGridViewEditProcedure.IsCurrentCellDirty)
            {
                currentSpecsDataGridViewEditProcedure.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (currentSpecsDataGridViewEditProcedure.Columns[e.ColumnIndex].Name == "")
                {
                    DataTable dt = (DataTable)currentSpecsDataGridViewEditProcedure.DataSource;
                    string procedureName = procedureToChangeComboBoxEditProcedureForm.Text;
                    string filePath = Path.Combine(extendedProcedurePath, procedureName + "\\SpecsAssociated.txt");
                    File.Delete(filePath);
                    string newFilePath = Path.Combine(extendedProcedurePath, procedureName + "\\SpecsAssociated.txt");
                    if (!File.Exists(newFilePath))
                    {
                        using (StreamWriter sw = File.CreateText(newFilePath))
                        {
                            foreach (DataRow dataRow in dt.Rows)
                            {
                                if (bool.Parse(dataRow["Select"].ToString()) == true)
                                {
                                    sw.WriteLine(dataRow["Spec Name"].ToString());

                                }
                            }
                        }
                    }
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)currentSpecsDataGridViewEditProcedure.Rows[e.RowIndex].Cells[""];
                    currentSpecsDataGridViewEditProcedure.Invalidate();
                }
            }

        }
    }
}

