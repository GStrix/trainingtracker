﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;


namespace TrainingTrackerUIForms
{
    public partial class ErrorsForm : Form
    {
        public ErrorsForm()
        {
            InitializeComponent();

            dataGridView1.AutoGenerateColumns = false;

            var hubFormReference = new HubForm();
            var associateList = hubFormReference.LoadAssociates();
            var associateStrings = hubFormReference.AssociateToString(associateList);
            var procedureErrorList = hubFormReference.LoadErrorTitles();
            string blankProcedure = "";
            string blankAssociate = "";
            procedureErrorList.Add(blankProcedure);
            associateStrings.Add(blankAssociate);
            List<ErrorsModel> errors = LoadErrors();
            associateComboBoxErrorsForm.DataSource = associateStrings;
            procedureComboBoxErrorsForm.DataSource = procedureErrorList;
            associateComboBoxErrorsForm.SelectedItem = null;
            procedureComboBoxErrorsForm.SelectedItem = null;

            DataTable dt = new DataTable();
            dt.Columns.Add("Select", typeof(bool));
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Severity", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Associate Reporting", typeof(string));
            dt.Columns.Add("Procedure", typeof(string));
            dt = FillErrorDataTable(errors, dt);


            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.HeaderText = "Select";
            select.DataPropertyName = "Select";

            DataGridViewTextBoxColumn date = new DataGridViewTextBoxColumn();
            date.HeaderText = "Date";
            date.DataPropertyName = "Date";

            DataGridViewTextBoxColumn employeeName = new DataGridViewTextBoxColumn();
            employeeName.HeaderText = "Employee Name";
            employeeName.DataPropertyName = "Employee Name";

            DataGridViewTextBoxColumn procedure = new DataGridViewTextBoxColumn();
            procedure.HeaderText = "Procedure";
            procedure.DataPropertyName = "Procedure";

            DataGridViewTextBoxColumn description = new DataGridViewTextBoxColumn();
            description.HeaderText = "Description";
            description.DataPropertyName = "Description";

            DataGridViewTextBoxColumn severity = new DataGridViewTextBoxColumn();
            severity.HeaderText = "Severity";
            severity.DataPropertyName = "Severity";

            DataGridViewTextBoxColumn associateReporting = new DataGridViewTextBoxColumn();
            associateReporting.HeaderText = "Associate Reporting";
            associateReporting.DataPropertyName = "Associate Reporting";

            dataGridView1.DataSource = dt;
            dataGridView1.Columns.AddRange(select);
            dataGridView1.Columns.AddRange(employeeName);
            dataGridView1.Columns.AddRange(date);
            dataGridView1.Columns.AddRange(procedure);
            dataGridView1.Columns.AddRange(description);
            dataGridView1.Columns.AddRange(severity);
            dataGridView1.Columns.AddRange(associateReporting);
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void BackButtonErrors_Click(object sender, EventArgs e)
        {
            var hubForm = new HubForm();
            hubForm.Show();
            this.Close();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SubmitNewErrorButtonErrorsForm_Click(object sender, EventArgs e)
        {
            var submitErrorForm = new SubmitErrorForm();
            submitErrorForm.Show();
            this.Close();
        }

        private DataTable FillErrorDataTable(List<ErrorsModel> errors, DataTable table)
        {
            DataRow dataRow = null;
            foreach (ErrorsModel err in errors)
            {
                dataRow = table.NewRow();
                dataRow["Select"] = false;
                dataRow["Date"] = err.ErrorDate;
                dataRow["Employee Name"] = err.AssociateResponsible;
                dataRow["Severity"] = err.Severity;
                dataRow["Description"] = err.ErrorDescription;
                dataRow["Associate Reporting"] = err.AssociateReporting;
                dataRow["Procedure"] = err.ProcedureError;
                table.Rows.Add(dataRow);
            }
            return table;
        }

        public List<ErrorsModel> LoadErrors()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedErrorsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Errors");
            var errorList = new List<ErrorsModel>();
            string[] fileArray = Directory.GetFiles(extendedErrorsPath);
            foreach (var file in fileArray)
            {
                    string[] lines = File.ReadAllLines(file);
                    var error = new ErrorsModel();
                    error.AssociateResponsible = lines[0];
                    error.ProcedureError = lines[3];
                    error.Severity = lines[2];
                    error.ErrorDescription = lines[4];
                    var dateTime = DateTime.Parse(lines[1]);
                    error.ErrorDate = dateTime;
                    error.AssociateReporting = lines[5];
                    errorList.Add(error);
            }
            return errorList;
        }

        public List<ErrorsModel> LoadAssociateErrors(string asscName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedErrorsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Errors");
            var errorList = new List<ErrorsModel>();
            string[] fileArray = Directory.GetFiles(extendedErrorsPath);
            foreach (var file in fileArray)
            {
                var error = new ErrorsModel();
                string[] lines = File.ReadAllLines(file);
                error.AssociateResponsible = lines[0];
                error.ProcedureError = lines[3];
                error.Severity = lines[2];
                error.ErrorDescription = lines[4];
                var dateTime = DateTime.Parse(lines[1]);
                error.ErrorDate = dateTime;
                error.AssociateReporting = lines[5];
                if (error.AssociateResponsible == asscName)
                {
                    errorList.Add(error);
                }
            }
            return errorList;
        }

        public List<ErrorsModel> LoadProcedureErrors(string procedureName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedErrorsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Errors");
            var errorList = new List<ErrorsModel>();
            string[] fileArray = Directory.GetFiles(extendedErrorsPath);
            foreach (var file in fileArray)
            {
                var error = new ErrorsModel();
                string[] lines = File.ReadAllLines(file);
                error.AssociateResponsible = lines[0];
                error.ProcedureError = lines[3];
                error.Severity = lines[2];
                error.ErrorDescription = lines[4];
                var dateTime = DateTime.Parse(lines[1]);
                error.ErrorDate = dateTime;
                error.AssociateReporting = lines[5];
                if (error.ProcedureError == procedureName)
                {
                    errorList.Add(error);
                }
            }
            return errorList;
        }

        public List<ErrorsModel> LoadAssociateProcedureErrors(string procedureName, string associateName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedErrorsPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Errors");
            var errorList = new List<ErrorsModel>();
            string[] fileArray = Directory.GetFiles(extendedErrorsPath);
            foreach (var file in fileArray)
            {
                var error = new ErrorsModel();
                string[] lines = File.ReadAllLines(file);
                error.AssociateResponsible = lines[0];
                error.ProcedureError = lines[3];
                error.Severity = lines[2];
                error.ErrorDescription = lines[4];
                var dateTime = DateTime.Parse(lines[1]);
                error.ErrorDate = dateTime;
                error.AssociateReporting = lines[5];
                if (error.ProcedureError == procedureName && error.AssociateResponsible == associateName)
                {
                    errorList.Add(error);
                }
            }
            return errorList;
        }



        private void AssociateComboBoxErrorsForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var selectedErrors = new List<ErrorsModel>();
            if (associateComboBoxErrorsForm.SelectedItem != null && procedureComboBoxErrorsForm.SelectedItem != null)
            {
                if (associateComboBoxErrorsForm.SelectedItem.ToString() != "" && procedureComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadAssociateProcedureErrors(procedureComboBoxErrorsForm.SelectedItem.ToString(), associateComboBoxErrorsForm.SelectedItem.ToString());
                }

                else if (associateComboBoxErrorsForm.SelectedItem.ToString() == "" && procedureComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadErrors();
                }

                else if (associateComboBoxErrorsForm.SelectedItem.ToString() != "" && procedureComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadAssociateErrors(associateComboBoxErrorsForm.SelectedItem.ToString());
                }

                else if (associateComboBoxErrorsForm.SelectedItem.ToString() == "" && procedureComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadProcedureErrors(procedureComboBoxErrorsForm.SelectedItem.ToString());
                }
            }
            else if (associateComboBoxErrorsForm.SelectedItem != null)
            {
                if(associateComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadAssociateErrors(associateComboBoxErrorsForm.SelectedItem.ToString());
                }
                else if (associateComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadErrors();
                }
            }
            else
            {
                selectedErrors = LoadErrors();
            }
            DataTable newDT = new DataTable();
            newDT.Columns.Add("Select", typeof(bool));
            newDT.Columns.Add("Date", typeof(DateTime));
            newDT.Columns.Add("Employee Name", typeof(string));
            newDT.Columns.Add("Severity", typeof(string));
            newDT.Columns.Add("Description", typeof(string));
            newDT.Columns.Add("Associate Reporting", typeof(string));
            newDT.Columns.Add("Procedure", typeof(string));
            newDT = FillErrorDataTable(selectedErrors, newDT);
            dataGridView1.DataSource = newDT;               

        }

        private void ProcedureComboBoxErrorsForm_SelectedIndexChanged(object sender, EventArgs e)
        {
             //TODO: FIX THIS, IT BROKE
             
             var selectedErrors = new List<ErrorsModel>();
            if (associateComboBoxErrorsForm.SelectedItem != null && procedureComboBoxErrorsForm.SelectedItem != null)
            {
                if (associateComboBoxErrorsForm.SelectedItem.ToString() != "" && procedureComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadAssociateProcedureErrors(procedureComboBoxErrorsForm.SelectedItem.ToString(), associateComboBoxErrorsForm.SelectedItem.ToString());
                }
                else if (associateComboBoxErrorsForm.SelectedItem.ToString() == "" && procedureComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadErrors();
                }

                else if (associateComboBoxErrorsForm.SelectedItem.ToString() != "" && procedureComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadAssociateErrors(associateComboBoxErrorsForm.SelectedItem.ToString());
                }

                else if (associateComboBoxErrorsForm.SelectedItem.ToString() == "" && procedureComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadProcedureErrors(procedureComboBoxErrorsForm.SelectedItem.ToString());
                }
            }
            else if (procedureComboBoxErrorsForm.SelectedItem != null)
            {
                if (procedureComboBoxErrorsForm.SelectedItem.ToString() != "")
                {
                    selectedErrors = LoadProcedureErrors(procedureComboBoxErrorsForm.SelectedItem.ToString());
                }
                else if(procedureComboBoxErrorsForm.SelectedItem.ToString() == "")
                {
                    selectedErrors = LoadErrors();
                }
            }
            else
            {
                selectedErrors = LoadErrors();
            }
            
             DataTable dt = new DataTable();
             dt.Columns.Add("Select", typeof(bool));
             dt.Columns.Add("Date", typeof(DateTime));
             dt.Columns.Add("Employee Name", typeof(string));
             dt.Columns.Add("Severity", typeof(string));
             dt.Columns.Add("Description", typeof(string));
             dt.Columns.Add("Associate Reporting", typeof(string));
             dt.Columns.Add("Procedure", typeof(string));
             dt = FillErrorDataTable(selectedErrors, dt);
             dataGridView1.DataSource = dt;              
         }

        private void SendReportButtonErrorsForm_Click(object sender, EventArgs e)
        {
            //TODO: SEND TO EXCEL SHEET
        }

        public void SendToExcel(DataTable dataForExcel)
        {
            var excelApp = new Excel.Application();
            excelApp.Visible = true;
            excelApp.Workbooks.Add();
            Excel._Worksheet worksheet = (Excel.Worksheet)excelApp.ActiveSheet;
            worksheet.Cells[1, "A"] = "Date";
            worksheet.Cells[1, "B"] = "Employee Name";
            worksheet.Cells[1, "C"] = "Severity";
            worksheet.Cells[1, "D"] = "Description";
            worksheet.Cells[1, "E"] = "Associate Reporting";
            worksheet.Cells[1, "F"] = "Procedure";
            var rows = 1;
            foreach (DataRow dRow in dataForExcel.Rows)
            {
                rows++;
                worksheet.Cells[rows, "A"] = dRow["Date"].ToString();
                worksheet.Cells[rows, "B"] = dRow["Employee Name"].ToString();
                worksheet.Cells[rows, "C"] = dRow["Severity"].ToString();
                worksheet.Cells[rows, "D"] = dRow["Severity"].ToString();
                worksheet.Cells[rows, "E"] = dRow["Description"].ToString();
                worksheet.Cells[rows, "F"] = dRow["Procedure"].ToString();
            }
            for (int i = 1; i < 7; i++)
            {
                ((Excel.Range)worksheet.Columns[i]).AutoFit();
            }
            
        }
    }
 }
