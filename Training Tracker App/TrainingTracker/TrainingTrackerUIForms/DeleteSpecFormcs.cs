﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainingTrackerUIForms
{
    public partial class DeleteSpecForm : Form
    {
        public bool confirmDelete { get; set; }
        public DeleteSpecForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.confirmDelete = false;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if(deleteSpecConfirmationCheckBox.Checked)
            {
                this.confirmDelete = true;
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }
    }
}
