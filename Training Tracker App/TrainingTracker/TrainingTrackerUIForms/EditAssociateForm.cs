﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainingTrackerLibrary;
using System.IO;

namespace TrainingTrackerUIForms
{
    public partial class EditAssociateForm : Form
    {
        public AssociateModel currentAssociate;
        public EditAssociateForm()
        {
            InitializeComponent();
            var hubFormReference = new HubForm();
            List<String> supervisors = new List<String>();
            supervisors = hubFormReference.LoadSupervisors();
            supervisorComboBoxEditAssociate.DataSource = supervisors;
            List<AssociateModel> associateModels = new List<AssociateModel>();
            associateModels = hubFormReference.LoadAssociates();
            List<String> associateStrings = new List<String>();
            associateStrings = hubFormReference.AssociateToString(associateModels);
            associateComboBoxEditAssociate.DataSource = associateStrings;
            List<String> jobTitles = new List<String>();
            jobTitles = hubFormReference.LoadJobTitles();
            jobTitlecomboBoxEditAssociate.DataSource = jobTitles;
            
        }

        public void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var hubFormReference = new HubForm();
            List<AssociateModel> associatesList = new List<AssociateModel>();
            List<String> selectedAssociates = new List<String>();
            associatesList = hubFormReference.LoadAssociates();
            foreach (var assc in associatesList) if (assc.supervisor == supervisorComboBoxEditAssociate.Text)
                {
                    selectedAssociates.Add(assc.FirstName + " " + assc.LastName);
                }
            associateComboBoxEditAssociate.DataSource = selectedAssociates;
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e) //On change associate, Admin/Supervisor not changing when associate changes.
        {
            var hubFormReference = new HubForm();
            List<String> selectedAssociate = new List<String>();
            List<AssociateModel> selectedAssociateModel = new List<AssociateModel>();
            selectedAssociate.Add(associateComboBoxEditAssociate.Text);
            selectedAssociateModel = hubFormReference.StringToAssociate(selectedAssociate);
            reportsToComboBoxEditAssociate.DataSource = hubFormReference.LoadSupervisors();
            firstNameTextBoxEditAssociate.Text = selectedAssociateModel[0].FirstName;
            lastNameTextBoxEditAssociateForm.Text = selectedAssociateModel[0].LastName;
            if (selectedAssociateModel[0].SupervisorStatus)
            {
                supervisorCheckBoxEditAssociate.CheckState = CheckState.Checked;
            }
            else
                supervisorCheckBoxEditAssociate.CheckState = CheckState.Unchecked;
            if (selectedAssociateModel[0].AdminAccess)
            {
                adminCheckBoxEditAssociate.CheckState = CheckState.Checked;
            }
            else
                adminCheckBoxEditAssociate.CheckState = CheckState.Unchecked;
            dateOfTrainingCalendarEditAssociate.SelectionStart = DateTime.Parse(selectedAssociateModel[0].StartDate);
            dateOfTrainingCalendarEditAssociate.SelectionEnd = DateTime.Parse(selectedAssociateModel[0].StartDate);
            employeeIDtextBoxEditAssociate.Text = selectedAssociateModel[0].employeeId;
            jobTitlecomboBoxEditAssociate.Text = selectedAssociateModel[0].jobTitle;
            employeeEmailTextBoxEditAssociate.Text = selectedAssociateModel[0].EmailAddress;
            reportsToComboBoxEditAssociate.Text = selectedAssociateModel[0].supervisor;
            currentAssociate = selectedAssociateModel[0];
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }


        private void EditAssociateForm_Load(object sender, EventArgs e)
        {

        }

        private void BackButtonEditAssociate_Click(object sender, EventArgs e)
        {
            var hubForm = new HubForm();
            hubForm.Show();
            this.Close();
        }

        private void NewAssociateButtonEditAssociate_Click(object sender, EventArgs e)
        {
            var addNewAssociateForm = new AddNewAssociateForm();
            addNewAssociateForm.Show();
            this.Close();
        }

        public void UpdateAssociateSpecs (List<String> specs, List<AssociateModel> associates)
        {
            
            foreach (AssociateModel assc in associates)
            {
                foreach (String spec in specs)
                {
                    bool contained = false;
                    for (int i=0; i < assc.Specs.Count; i++)
                    {
                        if (assc.Specs[i].SpecName == spec)
                        {
                            contained = true;
                        }
                    }
                    if (!contained)
                    {
                        string folderPath = assc.FirstName + " " + assc.LastName;
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                        var extendedPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
                        string specFilePath = Path.Combine(extendedPath, folderPath + "\\Specs");
                        string textFilePath = specFilePath + "\\" + spec + ".txt";
                        using (StreamWriter sw = File.CreateText(textFilePath));
                    }
                }

                foreach (SpecModel asscSpec in assc.Specs)
                {
                    bool contained = false;
                    {
                        for (int j=0; j < specs.Count; j++)
                        {
                            if (specs[j] == asscSpec.SpecName)
                            {
                                contained = true;
                            }
                        }
                        if (!contained)
                        {
                            string folderPath = assc.FirstName + " " + assc.LastName;
                            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                            var extendedPath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
                            string specFilePath = Path.Combine(extendedPath, folderPath + "\\Specs");
                            string textFilePath = specFilePath + "\\" + asscSpec.SpecName + ".txt";
                            File.Delete(textFilePath);
                        }
                    }
                }
            }
        }

        private void SaveButtonEditAssociate_Click(object sender, EventArgs e) //supervisor and admin box not being updated correctly for each employee
        {
            var hubFormReference = new HubForm();
            AssociateModel updatedAssociate = new AssociateModel();
            updatedAssociate.FirstName = firstNameTextBoxEditAssociate.Text;
            updatedAssociate.LastName = lastNameTextBoxEditAssociateForm.Text;
            updatedAssociate.jobTitle = jobTitlecomboBoxEditAssociate.Text;
            updatedAssociate.StartDate = dateOfTrainingCalendarEditAssociate.SelectionStart.ToShortDateString();
            updatedAssociate.SupervisorStatus = supervisorCheckBoxEditAssociate.Checked;
            updatedAssociate.supervisor = supervisorComboBoxEditAssociate.SelectedItem.ToString();
            updatedAssociate.employeeId = employeeIDtextBoxEditAssociate.Text;
            updatedAssociate.EmailAddress = employeeEmailTextBoxEditAssociate.Text;
            updatedAssociate.Password = currentAssociate.Password;
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            string[] specFilesArray = Directory.GetFiles(extendedAssociatePath + "\\" + updatedAssociate.FirstName + " " + updatedAssociate.LastName + "\\Specs");
            foreach (var file in specFilesArray)
            {
                var specModel = new SpecModel();
                string[] lines = File.ReadAllLines(file);
                specModel.SpecName = Path.GetFileNameWithoutExtension(file);
                specModel.SpecCompletionDate = lines[0];
                specModel.RevisionVersion = lines[1];
                updatedAssociate.Specs.Add(specModel);
            }
            string oldFolderName = currentAssociate.FirstName + " " + currentAssociate.LastName;
            string oldFilePath = Path.Combine(extendedAssociatePath, oldFolderName);
            Directory.Delete(oldFilePath, true);
            string newFolderName = updatedAssociate.FirstName + " " + updatedAssociate.LastName;
            string newFilePath = Path.Combine(extendedAssociatePath, newFolderName + "\\Data.txt");
            string newSpecFolderPath = Path.Combine(extendedAssociatePath, newFolderName + "\\Specs");
            Directory.CreateDirectory(newSpecFolderPath);
            Directory.CreateDirectory(Path.Combine(extendedAssociatePath, newFolderName));
            if (!File.Exists(newFilePath))
            {
                using (StreamWriter sw = File.CreateText(newFilePath))
                {
                    sw.WriteLine(updatedAssociate.FirstName);
                    sw.WriteLine(updatedAssociate.LastName);
                    sw.WriteLine(updatedAssociate.employeeId);
                    sw.WriteLine(updatedAssociate.EmailAddress);
                    sw.WriteLine(updatedAssociate.StartDate);
                    sw.WriteLine(updatedAssociate.supervisor);
                    if (updatedAssociate.SupervisorStatus)
                    {
                        sw.WriteLine("Y");
                    }
                    else
                        sw.WriteLine("N");
                    if (updatedAssociate.AdminAccess)
                    {
                        sw.WriteLine("Y");
                    }
                    else
                        sw.WriteLine("N");
                    sw.WriteLine(updatedAssociate.Password);
                    sw.WriteLine(updatedAssociate.jobTitle);

                }
            }
            foreach(SpecModel spec in updatedAssociate.Specs)
            {
                string newSpecFilePath = Path.Combine(newSpecFolderPath, spec.SpecName + ".txt");
                using (StreamWriter sw = File.CreateText(newSpecFilePath))
                {
                    sw.WriteLine(spec.SpecCompletionDate);
                    sw.WriteLine(spec.RevisionVersion);
                }
            }

            currentAssociate = updatedAssociate;
            List<String> supervisors = new List<String>();
            supervisors = hubFormReference.LoadSupervisors();
            supervisorComboBoxEditAssociate.DataSource = supervisors;
            List<AssociateModel> associateModels = new List<AssociateModel>();
            associateModels = hubFormReference.LoadAssociates();
            List<String> associateStrings = new List<String>();
            associateStrings = hubFormReference.AssociateToString(associateModels);
            associateComboBoxEditAssociate.DataSource = associateStrings;
            ClearForm();
        }

        private void DeleteAssociateButtonEditAssociate_Click(object sender, EventArgs e)
        {
            var deleteConfirmationForm = new DeleteAssociateForm();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedAssociatePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\Associates");
            var result = deleteConfirmationForm.ShowDialog();
            if (result == DialogResult.Yes)
            {
                string deleteFolderName = currentAssociate.FirstName + " " + currentAssociate.LastName;
                string deleteFilePath = Path.Combine(extendedAssociatePath, deleteFolderName);
                Directory.Delete(deleteFilePath, true);
                ClearForm(); 
                
            }

        }

        private void ClearForm()
        {
            firstNameTextBoxEditAssociate.Clear();
            lastNameTextBoxEditAssociateForm.Clear();
            employeeEmailTextBoxEditAssociate.Clear();
            employeeIDtextBoxEditAssociate.Clear();
            adminCheckBoxEditAssociate.Checked = false;
            supervisorCheckBoxEditAssociate.Checked = false;
        }
    }
}
