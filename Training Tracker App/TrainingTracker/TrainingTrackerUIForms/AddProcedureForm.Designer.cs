﻿namespace TrainingTrackerUIForms
{
    partial class AddProcedureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProcedureForm));
            this.newProcessNameLabelAddProcedure = new System.Windows.Forms.Label();
            this.specToAddComboBoxAddProcedure = new System.Windows.Forms.ComboBox();
            this.specToAddlabelAddProcedure = new System.Windows.Forms.Label();
            this.specToAddButtonAddProcedure = new System.Windows.Forms.Button();
            this.specsToAddDataGridViewAddProcedure = new System.Windows.Forms.DataGridView();
            this.completeProcedureButtonAddProcedure = new System.Windows.Forms.Button();
            this.backButtonAddProcedure = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.procedureNameTextBoxAddProcedure = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.specsToAddDataGridViewAddProcedure)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // newProcessNameLabelAddProcedure
            // 
            this.newProcessNameLabelAddProcedure.AutoSize = true;
            this.newProcessNameLabelAddProcedure.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newProcessNameLabelAddProcedure.ForeColor = System.Drawing.Color.White;
            this.newProcessNameLabelAddProcedure.Location = new System.Drawing.Point(31, 67);
            this.newProcessNameLabelAddProcedure.Name = "newProcessNameLabelAddProcedure";
            this.newProcessNameLabelAddProcedure.Size = new System.Drawing.Size(277, 30);
            this.newProcessNameLabelAddProcedure.TabIndex = 1;
            this.newProcessNameLabelAddProcedure.Text = "Drag Procedure Form Here";
            this.newProcessNameLabelAddProcedure.Click += new System.EventHandler(this.NewProcessNameLabelAddProcedure_Click);
            // 
            // specToAddComboBoxAddProcedure
            // 
            this.specToAddComboBoxAddProcedure.FormattingEnabled = true;
            this.specToAddComboBoxAddProcedure.Location = new System.Drawing.Point(48, 225);
            this.specToAddComboBoxAddProcedure.Name = "specToAddComboBoxAddProcedure";
            this.specToAddComboBoxAddProcedure.Size = new System.Drawing.Size(262, 21);
            this.specToAddComboBoxAddProcedure.TabIndex = 2;
            // 
            // specToAddlabelAddProcedure
            // 
            this.specToAddlabelAddProcedure.AutoSize = true;
            this.specToAddlabelAddProcedure.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specToAddlabelAddProcedure.ForeColor = System.Drawing.Color.White;
            this.specToAddlabelAddProcedure.Location = new System.Drawing.Point(346, 219);
            this.specToAddlabelAddProcedure.Name = "specToAddlabelAddProcedure";
            this.specToAddlabelAddProcedure.Size = new System.Drawing.Size(132, 30);
            this.specToAddlabelAddProcedure.TabIndex = 3;
            this.specToAddlabelAddProcedure.Text = "Spec to Add";
            this.specToAddlabelAddProcedure.Click += new System.EventHandler(this.Label1_Click);
            // 
            // specToAddButtonAddProcedure
            // 
            this.specToAddButtonAddProcedure.Location = new System.Drawing.Point(619, 198);
            this.specToAddButtonAddProcedure.Name = "specToAddButtonAddProcedure";
            this.specToAddButtonAddProcedure.Size = new System.Drawing.Size(174, 77);
            this.specToAddButtonAddProcedure.TabIndex = 4;
            this.specToAddButtonAddProcedure.Text = "Add Spec to Procedure";
            this.specToAddButtonAddProcedure.UseVisualStyleBackColor = true;
            this.specToAddButtonAddProcedure.Click += new System.EventHandler(this.SpecToAddButtonAddProcedure_Click);
            // 
            // specsToAddDataGridViewAddProcedure
            // 
            this.specsToAddDataGridViewAddProcedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.specsToAddDataGridViewAddProcedure.Location = new System.Drawing.Point(32, 298);
            this.specsToAddDataGridViewAddProcedure.Name = "specsToAddDataGridViewAddProcedure";
            this.specsToAddDataGridViewAddProcedure.RowHeadersWidth = 51;
            this.specsToAddDataGridViewAddProcedure.RowTemplate.Height = 24;
            this.specsToAddDataGridViewAddProcedure.Size = new System.Drawing.Size(919, 336);
            this.specsToAddDataGridViewAddProcedure.TabIndex = 5;
            // 
            // completeProcedureButtonAddProcedure
            // 
            this.completeProcedureButtonAddProcedure.Location = new System.Drawing.Point(838, 655);
            this.completeProcedureButtonAddProcedure.Name = "completeProcedureButtonAddProcedure";
            this.completeProcedureButtonAddProcedure.Size = new System.Drawing.Size(113, 62);
            this.completeProcedureButtonAddProcedure.TabIndex = 6;
            this.completeProcedureButtonAddProcedure.Text = "Complete Procedure";
            this.completeProcedureButtonAddProcedure.UseVisualStyleBackColor = true;
            this.completeProcedureButtonAddProcedure.Click += new System.EventHandler(this.CompleteProcedureButtonAddProcedure_Click);
            // 
            // backButtonAddProcedure
            // 
            this.backButtonAddProcedure.Location = new System.Drawing.Point(12, 681);
            this.backButtonAddProcedure.Name = "backButtonAddProcedure";
            this.backButtonAddProcedure.Size = new System.Drawing.Size(84, 36);
            this.backButtonAddProcedure.TabIndex = 7;
            this.backButtonAddProcedure.Text = "Back";
            this.backButtonAddProcedure.UseVisualStyleBackColor = true;
            this.backButtonAddProcedure.Click += new System.EventHandler(this.BackButtonAddProcedure_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.newProcessNameLabelAddProcedure);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 164);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // procedureNameTextBoxAddProcedure
            // 
            this.procedureNameTextBoxAddProcedure.Location = new System.Drawing.Point(512, 79);
            this.procedureNameTextBoxAddProcedure.Name = "procedureNameTextBoxAddProcedure";
            this.procedureNameTextBoxAddProcedure.Size = new System.Drawing.Size(365, 20);
            this.procedureNameTextBoxAddProcedure.TabIndex = 9;
            // 
            // AddProcedureForm
            // 
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1003, 729);
            this.Controls.Add(this.procedureNameTextBoxAddProcedure);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.backButtonAddProcedure);
            this.Controls.Add(this.completeProcedureButtonAddProcedure);
            this.Controls.Add(this.specsToAddDataGridViewAddProcedure);
            this.Controls.Add(this.specToAddButtonAddProcedure);
            this.Controls.Add(this.specToAddlabelAddProcedure);
            this.Controls.Add(this.specToAddComboBoxAddProcedure);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddProcedureForm";
            this.Text = "AddProcedureForm";
            ((System.ComponentModel.ISupportInitialize)(this.specsToAddDataGridViewAddProcedure)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label procedureTitleLabelAddProcedureForm;
        private System.Windows.Forms.TextBox procedureTitleTextBoxAddProcedureForm;
        private System.Windows.Forms.Label expirationDateLabelAddProcedureForm;
        private System.Windows.Forms.TextBox expirationDateDayTextBoxAddProcedureForm;
        private System.Windows.Forms.TextBox expirationDateMonthTextBoxAddProcedureForm;
        private System.Windows.Forms.TextBox expirationDateYearTextBoxAddProcedureForm;
        private System.Windows.Forms.Label expirationDateDaysLabelAddProcedureForm;
        private System.Windows.Forms.Label expirationDateMonthsLabelAddProcedureForm;
        private System.Windows.Forms.Label expirationDateYearsLabelAddProcedureForm;
        private System.Windows.Forms.Button addProcedureButtonAddProcedure;
        private System.Windows.Forms.Label newProcessNameLabelAddProcedure;
        private System.Windows.Forms.ComboBox specToAddComboBoxAddProcedure;
        private System.Windows.Forms.Label specToAddlabelAddProcedure;
        private System.Windows.Forms.Button specToAddButtonAddProcedure;
        private System.Windows.Forms.DataGridView specsToAddDataGridViewAddProcedure;
        private System.Windows.Forms.Button completeProcedureButtonAddProcedure;
        private System.Windows.Forms.Button backButtonAddProcedure;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox procedureNameTextBoxAddProcedure;
    }
}