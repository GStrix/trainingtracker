﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TrainingTrackerLibrary;

namespace TrainingTrackerUIForms
{
    public partial class AddProcedureForm : Form
    {
        public AddProcedureForm()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(AddProcedureForm_DragEnter);
            this.DragDrop += new DragEventHandler(AddProcedureForm_DragDrop);

            var specUpdateFormRef = new SpecUpdateForm();


            specsToAddDataGridViewAddProcedure.AutoGenerateColumns = false;
            List<String> specList = specUpdateFormRef.LoadSpecs();
            specToAddComboBoxAddProcedure.DataSource = specList;
            List<String> toAddSpecs = new List<String>();

            DataTable dt = new DataTable();
            dt.Columns.Add("Select", typeof(bool));
            dt.Columns.Add("Spec Name", typeof(string));

            DataGridViewCheckBoxColumn select = new DataGridViewCheckBoxColumn();
            select.HeaderText = "Select";
            select.DataPropertyName = "Select";

            DataGridViewTextBoxColumn firstName = new DataGridViewTextBoxColumn();
            firstName.HeaderText = "Spec Name";
            firstName.DataPropertyName = "Spec Name";

            specsToAddDataGridViewAddProcedure.DataSource = dt;
            specsToAddDataGridViewAddProcedure.Columns.AddRange(select);
            specsToAddDataGridViewAddProcedure.Columns.AddRange(firstName);

            specsToAddDataGridViewAddProcedure.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void AddProcedureForm_DragEnter(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null)
            {
                e.Effect = DragDropEffects.Copy;
            }

        }

        private void AddProcedureForm_DragDrop(object sender, DragEventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures");
            var specUpdateForm = new SpecUpdateForm();
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null && files.Length == 1)
            {
                foreach (string file in files)
                {
                    string fileName = Path.GetFileName(file);
                    string folderName = Path.GetFileNameWithoutExtension(file);
                    Directory.CreateDirectory(Path.Combine(extendedProcedurePath, folderName));
                    string destinationName = Path.Combine(extendedProcedurePath, folderName + "\\" +fileName);
                    File.Move(file, destinationName);
                    procedureNameTextBoxAddProcedure.Text = folderName;

                }
            }
        }

        private DataTable FillSpecDataTable(string specName, DataTable dataTable)
        {
            if (dataTable == null)
            {
                dataTable = new DataTable();
                dataTable.Columns.Add("Select", typeof(bool));
                dataTable.Columns.Add("Spec Name", typeof(string));
            }
            DataRow dataRow = null;
            dataRow = dataTable.NewRow();
            dataRow["Select"] = false;
            dataRow["Spec Name"] = specName;
            dataTable.Rows.Add(dataRow);
            return dataTable;
        }



        private void AddProcedureButtonAddProcedure_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void NewProcessNameLabelAddProcedure_Click(object sender, EventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void SpecToAddButtonAddProcedure_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)specsToAddDataGridViewAddProcedure.DataSource;
            string specName = specToAddComboBoxAddProcedure.SelectedItem.ToString();
            dt = FillSpecDataTable(specName, dt);
            specsToAddDataGridViewAddProcedure.DataSource = dt;

        }

        private void CompleteProcedureButtonAddProcedure_Click(object sender, EventArgs e)
        {
            string folderName = procedureNameTextBoxAddProcedure.Text;
            var path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var extendedProcedurePath = Path.Combine(path, "Training Tracker App\\TrainingTracker\\TrainingForms\\Procedures\\");
            string filePath = Path.Combine(extendedProcedurePath, folderName + "\\SpecsAssociated.txt");
            if (!File.Exists(filePath))
            {
                using (StreamWriter sw = File.CreateText(filePath))
                {
                    foreach (DataGridViewRow row in specsToAddDataGridViewAddProcedure.Rows)
                    {
                        if (row.Cells[1].Value != null)
                        {
                            sw.WriteLine(row.Cells[1].Value.ToString());
                        }
                    }

                }
            }
            procedureNameTextBoxAddProcedure.Clear();
            specsToAddDataGridViewAddProcedure.DataSource = null;

        }

        private void BackButtonAddProcedure_Click(object sender, EventArgs e)
        {
            var procedureForm = new EditProcedureForm();
            procedureForm.Show();
            this.Close();

        }
    }
}
