﻿namespace TrainingTrackerUIForms
{
    partial class DeleteProcedureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.procedureComboBoxDeleteProcedure = new System.Windows.Forms.ComboBox();
            this.procedureLabelDeleteProcedure = new System.Windows.Forms.Label();
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure = new System.Windows.Forms.CheckBox();
            this.deleteButtonDeleteProcedure = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // procedureComboBoxDeleteProcedure
            // 
            this.procedureComboBoxDeleteProcedure.FormattingEnabled = true;
            this.procedureComboBoxDeleteProcedure.Location = new System.Drawing.Point(182, 35);
            this.procedureComboBoxDeleteProcedure.Name = "procedureComboBoxDeleteProcedure";
            this.procedureComboBoxDeleteProcedure.Size = new System.Drawing.Size(214, 39);
            this.procedureComboBoxDeleteProcedure.TabIndex = 0;
            // 
            // procedureLabelDeleteProcedure
            // 
            this.procedureLabelDeleteProcedure.AutoSize = true;
            this.procedureLabelDeleteProcedure.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.procedureLabelDeleteProcedure.Location = new System.Drawing.Point(32, 38);
            this.procedureLabelDeleteProcedure.Name = "procedureLabelDeleteProcedure";
            this.procedureLabelDeleteProcedure.Size = new System.Drawing.Size(128, 32);
            this.procedureLabelDeleteProcedure.TabIndex = 1;
            this.procedureLabelDeleteProcedure.Text = "Procedure:";
            // 
            // procedureDeleteConfirmationCheckBoxDeleteProcedure
            // 
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.AutoSize = true;
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.Location = new System.Drawing.Point(24, 153);
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.Name = "procedureDeleteConfirmationCheckBoxDeleteProcedure";
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.Size = new System.Drawing.Size(552, 36);
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.TabIndex = 2;
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.Text = "Are you sure you want to delete this procedure?";
            this.procedureDeleteConfirmationCheckBoxDeleteProcedure.UseVisualStyleBackColor = true;
            // 
            // deleteButtonDeleteProcedure
            // 
            this.deleteButtonDeleteProcedure.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButtonDeleteProcedure.Location = new System.Drawing.Point(204, 294);
            this.deleteButtonDeleteProcedure.Name = "deleteButtonDeleteProcedure";
            this.deleteButtonDeleteProcedure.Size = new System.Drawing.Size(167, 111);
            this.deleteButtonDeleteProcedure.TabIndex = 3;
            this.deleteButtonDeleteProcedure.Text = "Delete";
            this.deleteButtonDeleteProcedure.UseVisualStyleBackColor = true;
            // 
            // DeleteProcedureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(613, 464);
            this.Controls.Add(this.deleteButtonDeleteProcedure);
            this.Controls.Add(this.procedureDeleteConfirmationCheckBoxDeleteProcedure);
            this.Controls.Add(this.procedureLabelDeleteProcedure);
            this.Controls.Add(this.procedureComboBoxDeleteProcedure);
            this.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "DeleteProcedureForm";
            this.Text = "DeleteProcedureForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox procedureComboBoxDeleteProcedure;
        private System.Windows.Forms.Label procedureLabelDeleteProcedure;
        private System.Windows.Forms.CheckBox procedureDeleteConfirmationCheckBoxDeleteProcedure;
        private System.Windows.Forms.Button deleteButtonDeleteProcedure;
    }
}