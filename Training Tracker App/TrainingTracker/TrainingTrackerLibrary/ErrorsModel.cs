﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class ErrorsModel
    {
        /// <summary>
        /// Represents the date the error occurred.
        /// </summary>
        public DateTime ErrorDate { get; set; }

        /// <summary>
        /// Represents the name of the associate who reported the error
        /// </summary>
        public string AssociateReporting { get; set; }

        /// <summary>
        /// Represents the description of the error that occurred.
        /// </summary>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Represents the procedure the error occurred on.
        /// </summary>
        public string ProcedureError { get; set; }

        /// <summary>
        /// Represents the associate who made the error.
        /// </summary>
        public string AssociateResponsible { get; set; }

        /// <summary>
        /// Represents the severity of the error.
        /// </summary>
        public string Severity { get; set; }
    }
}
