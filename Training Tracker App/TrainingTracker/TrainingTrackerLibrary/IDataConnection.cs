﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public interface IDataConnection
    {
        AssociateModel CreateAssociate(AssociateModel model);
    }
}
