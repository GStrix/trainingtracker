﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class ProcedureModel
    {
        /// <summary>
        /// References the procedure name.
        /// </summary>
        public string ProcedureName { get; set; }

        /// <summary>
        /// References the proficiency level the associate has in that proficiency.
        /// </summary>
        //public int Proficiency { get; set; }

        /// <summary>
        /// References the date the associate learned the procedure.
        /// </summary>
        public string DateLearned { get; set; }

        /// <summary>
        /// References the amount of time that passes before an associate needs retraining.
        /// </summary>
        public List<SpecModel> Specs = new List<SpecModel>(); //{ get; set; }
    }
}
