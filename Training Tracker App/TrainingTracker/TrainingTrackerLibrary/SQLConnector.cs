﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class SQLConnector : IDataConnection
    {
        // TODO -- Make the CreateAssociate method actually save to the database.
        /// <summary>
        /// Saves a new associate to the database
        /// </summary>
        /// <param name="model">The associate information</param>
        /// <returns>The associate information, including the unique indentifier.</returns>
        public AssociateModel CreateAssociate(AssociateModel model)
        {
            model.Id = 1;

            return model;
        }
    }
}
