﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class BuildingModel
    {
        /// <summary>
        /// References a list of Members assigned to a building.
        /// </summary>
        public List<AssociateModel> TeamMembers { get; set; } = new List<AssociateModel>();

        /// <summary>
        /// References a name of a building
        /// </summary>
        public string BuildingName { get; set; }
    }
}
