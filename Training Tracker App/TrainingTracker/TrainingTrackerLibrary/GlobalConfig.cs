﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public static class GlobalConfig
    {
        public static List<IDataConnection> Connections { get; private set; } = new List<IDataConnection>();

        public static void InitializeConnections ( bool database, bool textFiles)
        {
            if(database == true)
            {
                // TODO -- Set up the SQL Connector properly.
                SQLConnector sql = new SQLConnector();
                Connections.Add(sql);
            }

            if(textFiles == true)
            {
                // TODO -- Set up the Text Connector properly.
                TextConnection text = new TextConnection();
                Connections.Add(text);
            }
        }
    }
}
