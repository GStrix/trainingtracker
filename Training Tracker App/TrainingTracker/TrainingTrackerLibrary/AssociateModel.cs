﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class AssociateModel
    {
        /// <summary>
        /// The unique identifier for the associate
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Represents associate's first name.
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Represents associate's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Represents associate's email address.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Represents associate's start date
        /// </summary>
        public String StartDate { get; set; }

        /// <summary>
        /// Represents the procedures that the associate knows
        /// </summary>
        public List<SpecModel> Specs = new List<SpecModel>();

        /// <summary>
        /// Represents if the associate has admin access.
        /// </summary>
        public bool AdminAccess { get; set; }

        /// <summary>
        /// Represents if the associate is a supervisor.
        /// </summary>
        public bool SupervisorStatus { get; set; }

        /// <summary>
        /// Represents the associate's password to access the training tracker.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The associate's employeeID
        /// </summary>
        public string employeeId { get; set; }

        /// <summary>
        /// The associate's supervisor
        /// </summary>
        public string supervisor { get; set; }

        /// <summary>
        /// The associate's job title
        /// </summary>
        public string jobTitle { get; set; }

        /// <summary>
        /// Represents the errors that the associate has made.
        /// </summary>
        //public List<ErrorsModel> AssociateErrors { get; set; } = new List<ErrorsModel>();
    }
}
