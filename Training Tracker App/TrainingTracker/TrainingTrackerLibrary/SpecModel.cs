﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingTrackerLibrary
{
    public class SpecModel
    {
        /// <summary>
        /// Represents name of the Spec.
        /// </summary>
        public string SpecName { get; set; }

        /// <summary>
        /// Represents date the spec was learned.
        /// </summary>
        public string SpecCompletionDate { get; set; }

        /// <summary>
        /// Represents date the spec was learned.
        /// </summary>
        public string RevisionVersion { get; set; }
    }
}
